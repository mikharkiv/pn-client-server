package ua.org.edu.ukma.pncs.app.event;

import lombok.Getter;

public class Event {
	@Getter
	private final Events eventType;
	@Getter
	private final Object[] data;

	public Event(Events type, Object... data) {
		this.eventType = type;
		this.data = data;
	}

	public Event(Events type) {
		this(type, (Object) null);
	}
}
