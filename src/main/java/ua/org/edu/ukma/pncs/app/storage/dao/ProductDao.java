package ua.org.edu.ukma.pncs.app.storage.dao;

import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;
import ua.org.edu.ukma.pncs.app.storage.dao.exception.ErrorHandler;
import ua.org.edu.ukma.pncs.app.storage.dao.exception.TransactionException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductDao implements Dao<Product> {
	public static final String TABLE_NAME = "Products";

	public static final String COLUMN_1_ID = "product_id";
	public static final String COLUMN_2_GROUP_ID = "group_id";
	public static final String COLUMN_3_NAME = "product_name";
	public static final String COLUMN_4_MANUFACTURER = "product_manufacturer";
	public static final String COLUMN_5_DESCRIPTION = "product_description";
	public static final String COLUMN_6_QUANTITY = "product_quantity";
	public static final String COLUMN_7_PRICE = "product_price";

	public static final String CREATE_TABLE = "create table if not exists " + TABLE_NAME + " ( '" +
		COLUMN_1_ID + "' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '" +
		COLUMN_2_GROUP_ID + "' INTEGER NOT NULL, '" +
		COLUMN_3_NAME + "' TEXT NOT NULL UNIQUE, '" +
		COLUMN_4_MANUFACTURER + "' TEXT NOT NULL, '" +
		COLUMN_5_DESCRIPTION + "' TEXT , '" +
		COLUMN_6_QUANTITY + "' INTEGER, '" +
		COLUMN_7_PRICE + "' REAL, " +
		"FOREIGN KEY (" + GroupDao.COLUMN_1_ID + ") REFERENCES " + GroupDao.TABLE_NAME + "(" + GroupDao.COLUMN_1_ID + ") ON DELETE CASCADE " +
		");";

	public static final String INSERT_PRODUCT = "INSERT INTO " + TABLE_NAME + "(" + COLUMN_2_GROUP_ID + ", " + COLUMN_3_NAME + ", " + COLUMN_4_MANUFACTURER + ", " + COLUMN_5_DESCRIPTION + ", " + COLUMN_6_QUANTITY + ", " + COLUMN_7_PRICE + ") VALUES (?,?,?,?,?,?)";

	public static final String JOIN_SELECT_PRODUCTS = "SELECT * FROM " + TABLE_NAME +
		" LEFT JOIN " + GroupDao.TABLE_NAME +
		" ON " + TABLE_NAME + "." + COLUMN_2_GROUP_ID + " = " + GroupDao.TABLE_NAME + "." + GroupDao.COLUMN_1_ID;

	public static final String DELETE_PRODUCT_BY_ID = "DELETE FROM " + TABLE_NAME + " WHERE " + COLUMN_1_ID + "=?";
	public static final String UPDATE_PRODUCT_BY_ID = "UPDATE " + TABLE_NAME + " SET  " + COLUMN_2_GROUP_ID + "=?, " + COLUMN_3_NAME + "=?, " + COLUMN_4_MANUFACTURER + "=?, " + COLUMN_5_DESCRIPTION + "=?, " + COLUMN_6_QUANTITY + "=?, " + COLUMN_7_PRICE + "=?  WHERE " + COLUMN_1_ID + "=? ";

	public static final String JOIN_GET_BY_ID = "SELECT * FROM " + TABLE_NAME +
		" LEFT JOIN " + GroupDao.TABLE_NAME +
		" ON " + TABLE_NAME + "." + COLUMN_2_GROUP_ID + " = " + GroupDao.TABLE_NAME + "." + GroupDao.COLUMN_1_ID +
		" WHERE " + COLUMN_1_ID + "=? ";

	@Deprecated
	public static final String DELETE_PRODUCT_BY_NAME = "DELETE FROM " + TABLE_NAME + " WHERE (" + COLUMN_2_GROUP_ID + "=? AND " + COLUMN_3_NAME + "=?)";

	@Deprecated
	public static final String UPDATE_PRODUCT_BY_NAME = "UPDATE " + TABLE_NAME + " SET  " + COLUMN_6_QUANTITY + "=?, " + COLUMN_7_PRICE + "=? WHERE (" + COLUMN_2_GROUP_ID + "=? AND " + COLUMN_3_NAME + "=?)";

	@Deprecated
	public static final String JOIN_GET_BY_NAME = "SELECT * FROM " + TABLE_NAME +
		" LEFT JOIN " + GroupDao.TABLE_NAME +
		" ON " + TABLE_NAME + "." + COLUMN_2_GROUP_ID + " = " + GroupDao.TABLE_NAME + "." + GroupDao.COLUMN_1_ID +
		" WHERE (" + GroupDao.COLUMN_2_NAME + "=? AND " + COLUMN_3_NAME + "=?) ";

	private final Connection connection;

	public ProductDao(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @param id
	 * @return Product from the table by id
	 */
	@Override
	public Optional<Product> get(int id) {
		try {
			PreparedStatement st = connection.prepareStatement(JOIN_GET_BY_ID);
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			Product resultProduct = null;
			if (res.next()) {
				resultProduct = new Product(res.getInt(COLUMN_1_ID), new Group(res.getInt(GroupDao.COLUMN_1_ID), res.getString(GroupDao.COLUMN_2_NAME), res.getString(GroupDao.COLUMN_3_DESCRIPTION)), res.getString(COLUMN_3_NAME), res.getString(COLUMN_4_MANUFACTURER), res.getString(COLUMN_5_DESCRIPTION), res.getInt(COLUMN_6_QUANTITY), res.getDouble(COLUMN_7_PRICE));
			}
			res.close();
			st.close();
			return Optional.ofNullable(resultProduct);
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			return Optional.empty();
		}
	}

	/**
	 * @return All products from the table
	 */
	@Override
	public List<Product> getAll() {
		try {
			PreparedStatement st = connection.prepareStatement(JOIN_SELECT_PRODUCTS);

			ResultSet res = st.executeQuery();
			ArrayList<Product> resultList = new ArrayList<>();

			while (res.next()) {
				resultList.add(new Product(res.getInt(COLUMN_1_ID), new Group(res.getInt(GroupDao.COLUMN_1_ID), res.getString(GroupDao.COLUMN_2_NAME), res.getString(GroupDao.COLUMN_3_DESCRIPTION)), res.getString(COLUMN_3_NAME), res.getString(COLUMN_4_MANUFACTURER), res.getString(COLUMN_5_DESCRIPTION), res.getInt(COLUMN_6_QUANTITY), res.getDouble(COLUMN_7_PRICE)));
			}
			res.close();
			st.close();
			return resultList;
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			return null;
		}
	}

	/**
	 * Save product
	 *
	 * @param product
	 * @throws TransactionException
	 */
	@Override
	public void save(Product product) throws TransactionException {
		try {
			Optional<Group> currentGroup = getGroup(product.getGroup().getName());
			if (!currentGroup.isPresent()) throw new TransactionException("Group not exists");
			PreparedStatement statement = connection.prepareStatement(INSERT_PRODUCT);
			statement.setInt(1, currentGroup.get().getId());
			statement.setString(2, product.getName());
			statement.setString(3, product.getManufacturer());
			statement.setString(4, product.getDescription());
			statement.setInt(5, product.getQuantity());
			statement.setDouble(6, product.getPrice());
			int result = statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException(e.getMessage());
		}
	}

	/**
	 * Update existing product in the table
	 *
	 * @throws TransactionException
	 */
	public void update(Product prod) throws TransactionException {
		try {
			Optional<Group> currentGroup = getGroup(prod.getGroup().getName());
			if (!currentGroup.isPresent()) throw new TransactionException("Group not exists");

			PreparedStatement statement = connection.prepareStatement(UPDATE_PRODUCT_BY_ID);
			statement.setInt(7, prod.getId());
			statement.setInt(1, currentGroup.get().getId());
			statement.setString(2, prod.getName());
			statement.setString(3, prod.getManufacturer());
			statement.setString(4, prod.getDescription());
			statement.setInt(5, prod.getQuantity());
			statement.setDouble(6, prod.getPrice());
			int result = statement.executeUpdate();
			statement.close();
			if (result < 0) throw new TransactionException("Transaction failed");
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * Delete product from the table
	 *
	 * @param id
	 * @throws TransactionException
	 */
	public void delete(int id) throws TransactionException {
		try {
			PreparedStatement statement = connection.prepareStatement(DELETE_PRODUCT_BY_ID);
			statement.setInt(1, id);
			int result = statement.executeUpdate();
			if (result <= 0) throw new TransactionException("Product has not been deleted");
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	private Optional<Group> getGroup(String groupName) {
		try {
			PreparedStatement st = connection.prepareStatement(GroupDao.GET_BY_NAME);
			st.setString(1, groupName);
			ResultSet res = st.executeQuery();
			Group resultGroup = null;
			if (res.next()) {
				resultGroup = new Group(res.getInt(GroupDao.COLUMN_1_ID), res.getString(GroupDao.COLUMN_2_NAME), res.getString(GroupDao.COLUMN_3_DESCRIPTION));
			}
			res.close();
			st.close();
			return Optional.ofNullable(resultGroup);
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			return Optional.empty();
		}
	}

	/**
	 * Clear the table
	 *
	 * @throws TransactionException
	 */
	@Override
	public void clear() throws TransactionException {
		try {
			Statement statement = connection.createStatement();
			int result = statement.executeUpdate("delete from " + TABLE_NAME);
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * @param groupName
	 * @param productName
	 * @return Product from the table by groupName and productName
	 */
	@Deprecated
	public Optional<Product> get(String groupName, String productName) {
		try {
			PreparedStatement st = connection.prepareStatement(JOIN_GET_BY_NAME);
			st.setString(1, groupName);
			st.setString(2, productName);
			ResultSet res = st.executeQuery();
			Product resultProduct = null;
			if (res.next()) {
				resultProduct = new Product(res.getInt(COLUMN_1_ID), new Group(res.getInt(GroupDao.COLUMN_1_ID), res.getString(GroupDao.COLUMN_2_NAME), res.getString(GroupDao.COLUMN_3_DESCRIPTION)), res.getString(COLUMN_3_NAME), res.getString(COLUMN_4_MANUFACTURER), res.getString(COLUMN_5_DESCRIPTION), res.getInt(COLUMN_6_QUANTITY), res.getDouble(COLUMN_7_PRICE));
			}
			res.close();
			st.close();
			return Optional.ofNullable(resultProduct);
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			return Optional.empty();
		}
	}

	/**
	 * Delete product from the table
	 *
	 * @param product
	 * @throws TransactionException
	 */
	@Override
	@Deprecated
	public void delete(Product product) throws TransactionException {
		try {
			Optional<Group> currentGroup = getGroup(product.getGroup().getName());
			if (!currentGroup.isPresent()) throw new TransactionException("Group not exists");
			PreparedStatement statement = connection.prepareStatement(DELETE_PRODUCT_BY_NAME);
			statement.setInt(1, currentGroup.get().getId());
			statement.setString(2, product.getName());
			int result = statement.executeUpdate();
			if (result <= 0) throw new TransactionException("Product has not been deleted");
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * Update existing product in the table
	 *
	 * @param product
	 * @param params
	 * @throws TransactionException
	 */
	@Override
	@Deprecated
	public void update(Product product, Object... params) throws TransactionException {
		try {
			if (params.length < 1) return;
			PreparedStatement statement = connection.prepareStatement(UPDATE_PRODUCT_BY_NAME);
			statement.setInt(3, product.getGroup().getId());
			statement.setString(4, product.getName());
			statement.setInt(1, params[0] == null ? product.getQuantity() : (Integer) params[0]);
			if (params.length >= 2)
				statement.setDouble(2, params[1] == null ? product.getPrice() : (Double) params[1]);
			else statement.setDouble(2, product.getPrice());
			int result = statement.executeUpdate();
			statement.close();
			if (result < 0) throw new TransactionException("Transaction failed");
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}
}
