package ua.org.edu.ukma.pncs.app.controller.model;

import com.google.common.primitives.UnsignedLong;
import lombok.Setter;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;

public abstract class AbstractMiddleware {
	@Setter
	private IMiddlewareController manager;
	private AbstractMiddleware next;

	protected abstract void handle(MiddlewareMessage p);

	public void pass(MiddlewareMessage p) {
		if (next != null)
			next.handle(p);
		else
			manager.handle(p);
	}

	public void result(Packet p) {
		if (manager != null)
			manager.result(p);
	}

	public void result(Message m) {
		if (manager != null)
			manager.result(new Packet((byte) 0, UnsignedLong.ZERO ,m));
	}

	public void error(Exception e) {
		if (manager != null)
			manager.onException(e);
	}

	public void setNext(AbstractMiddleware next) {
		this.next = next;
		next.setManager(manager);
	}
}
