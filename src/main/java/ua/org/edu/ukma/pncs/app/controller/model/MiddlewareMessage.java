package ua.org.edu.ukma.pncs.app.controller.model;

import lombok.Data;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.network.model.AbstractUser;

@Data
public class MiddlewareMessage {
	private Event event;
	private AbstractUser user;
	private Packet packet;

	private MiddlewareMessage() {}

	public MiddlewareMessage(Event event, AbstractUser user) {
		this.event = event;
		this.user = user;
	}

	public MiddlewareMessage(Packet packet, AbstractUser user) {
		this.packet = packet;
		this.user = user;
	}
}
