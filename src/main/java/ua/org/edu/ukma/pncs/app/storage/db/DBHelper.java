package ua.org.edu.ukma.pncs.app.storage.db;

import org.sqlite.SQLiteConfig;
import ua.org.edu.ukma.pncs.app.storage.dao.GroupDao;
import ua.org.edu.ukma.pncs.app.storage.dao.ProductDao;
import ua.org.edu.ukma.pncs.app.storage.dao.UserDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBHelper {
	//Additional TESTING options
	/**
	 * Flag for creating test db
	 */
	public static boolean isTest = false;
	/**
	 * Flag for dropping all tables before init
	 * ONLY FOR TESTING
	 */
	public static boolean dropTables = false;

	private Connection connection;
	volatile private static DBHelper instance;

	private DBHelper() {
		init();
	}

	private void createTables() {
		try {
			if (isTest && dropTables) {
				connection.createStatement().executeUpdate("DROP TABLE IF EXISTS " + UserDao.TABLE_NAME);
				connection.createStatement().executeUpdate("DROP TABLE IF EXISTS " + ProductDao.TABLE_NAME);
				connection.createStatement().executeUpdate("DROP TABLE IF EXISTS " + GroupDao.TABLE_NAME);
			}
			PreparedStatement usersTable = connection.prepareStatement(UserDao.CREATE_TABLE);
			int result = usersTable.executeUpdate();
			PreparedStatement groupsTable = connection.prepareStatement(GroupDao.CREATE_TABLE);
			result = groupsTable.executeUpdate();
			PreparedStatement productsTable = connection.prepareStatement(ProductDao.CREATE_TABLE);
			result = productsTable.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Wrong SQL Query");
		}
	}

	private void init() {
		try {
			Class.forName(DatabaseProperties.getProperty("DRIVER"));
			SQLiteConfig config = new SQLiteConfig();
			config.enforceForeignKeys(true);
			connection = DriverManager.getConnection(DatabaseProperties.getProperty("TYPE") + (isTest ? DatabaseProperties.getProperty("TEST_NAME") : DatabaseProperties.getProperty("TEST_NAME")), config.toProperties());
			createTables();
		} catch (ClassNotFoundException e) {
			System.out.println("JDBC Driver not found");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Wrong SQL query");
			e.printStackTrace();
		}
	}

	public static Connection getConnection() {
		DBHelper result = instance;
		if (result != null){
			try {
				if(result.connection.isClosed())
					result.init();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return result.connection;
		}
		synchronized (DBHelper.class) {
			if (instance == null) {
				instance = new DBHelper();
			}
			return instance.connection;
		}
	}

	public static void closeConnection() {
		if (instance != null) {
			try {
				instance.connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
