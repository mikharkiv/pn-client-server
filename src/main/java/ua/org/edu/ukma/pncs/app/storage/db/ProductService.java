package ua.org.edu.ukma.pncs.app.storage.db;

import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;
import ua.org.edu.ukma.pncs.app.storage.model.Utils;
import ua.org.edu.ukma.pncs.app.storage.dao.ProductDao;
import ua.org.edu.ukma.pncs.app.storage.dao.exception.TransactionException;

import java.sql.Connection;
import java.util.List;

public class ProductService {
	private final ProductDao productDao;

	public ProductService(Connection connection) {
		this.productDao = new ProductDao(connection);
	}

	/**
	 * Method for testing purposes
	 *
	 * @return
	 */
	synchronized public void clear() {
		try {
			productDao.clear();
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
		}
	}

	/**
	 * Method for testing purposes
	 *
	 * @return Product
	 */
	public Product getProduct(String groupName, String productName) {
		return productDao.get(groupName, productName).orElseGet(
			() -> Utils.emptyProduct());
	}

	public List<Product> getAll() {
		return productDao.getAll();
	}

	public Product getProduct(int id) {
		return productDao.get(id).orElseGet(
			() -> Utils.emptyProduct());
	}

	synchronized public DBService.Result deleteProduct(int id) {
		try {
			productDao.delete(id);
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

	synchronized public DBService.Result createProduct(Product product) {
		try {
			if(product.getQuantity() < 0)return DBService.Result.NEGATIVE_QUANTITY;
			if(product.getPrice() < 0)return DBService.Result.NEGATIVE_QUANTITY;
			productDao.save(product);
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

	synchronized public DBService.Result update(Product prod) {
		try {
			if(prod.getQuantity() < 0)return DBService.Result.NEGATIVE_QUANTITY;
			if(prod.getPrice() < 0)return DBService.Result.NEGATIVE_QUANTITY;
			productDao.update(prod);
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

	@Deprecated
	synchronized public DBService.Result createProduct(String productName, String groupName) {
		return createProduct(new Product(new Group(groupName), productName, 0, 0.));
	}

	@Deprecated
	synchronized public DBService.Result deleteProduct(Product product) {
		try {
			productDao.delete(new Product(product.getGroup(), product.getName(), product.getQuantity(), product.getPrice()));
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

	@Deprecated
	synchronized public DBService.Result deleteProduct(String productName, String groupName) {
		return deleteProduct(new Product(new Group(groupName), productName, 0, 0.));
	}

	@Deprecated
	public Integer howMany(String groupName, String productName) {
		return productDao.get(groupName, productName).orElse(Utils.emptyProduct()).getQuantity();
	}

	@Deprecated
	synchronized public DBService.Result subtractProducts(String groupName, String productName, Integer quantity) {
		if (quantity < 0) return DBService.Result.NEGATIVE_QUANTITY;
		Product currentProd = productDao.get(groupName, productName).orElse(Utils.emptyProduct());
		if (currentProd.getQuantity() < 0) return DBService.Result.EMPTY;
		if (currentProd.getQuantity() - quantity < 0) return DBService.Result.NOT_ENOUGH_PRODUCTS;
		try {
			productDao.update(currentProd, currentProd.getQuantity() - quantity);
		} catch (ClassCastException cce) {
			System.out.println("Wrong parameters");
			cce.printStackTrace();
			return DBService.Result.FAILED;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
		return DBService.Result.SUCCESS;
	}

	@Deprecated
	synchronized public DBService.Result plusProducts(String groupName, String productName, Integer quantity) {
		if (quantity < 0) return DBService.Result.NEGATIVE_QUANTITY;
		Product currentProd = productDao.get(groupName, productName).orElse(Utils.emptyProduct());
		if (currentProd.getQuantity() < 0) return DBService.Result.EMPTY;
		try {
			productDao.update(currentProd, currentProd.getQuantity() + quantity);
		} catch (ClassCastException cce) {
			System.out.println("Wrong parameters");
			cce.printStackTrace();
			return DBService.Result.FAILED;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
		return DBService.Result.SUCCESS;
	}

	@Deprecated
	synchronized public DBService.Result setPrice(String groupName, String productName, Double price) {
		if (price < 0) return DBService.Result.NEGATIVE_QUANTITY;
		Product currentProd = productDao.get(groupName, productName).orElse(Utils.emptyProduct());
		if (currentProd.getQuantity() < 0) return DBService.Result.EMPTY;
		try {
			productDao.update(currentProd, null, price);
		} catch (ClassCastException cce) {
			System.out.println("Wrong parameters");
			cce.printStackTrace();
			return DBService.Result.FAILED;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
		return DBService.Result.SUCCESS;
	}
}
