package ua.org.edu.ukma.pncs.gui.utils;

import javafx.scene.image.Image;
import javafx.stage.Stage;

public class IconManager {
	public static void setIcon(Stage stage){
		stage.getIcons().add(new Image(IconManager.class.getClassLoader().getResource("images/icon.png").toString()));
	}
}
