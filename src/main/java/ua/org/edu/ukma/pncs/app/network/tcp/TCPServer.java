package ua.org.edu.ukma.pncs.app.network.tcp;

import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.model.AbstractServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer extends AbstractServer {
	private final ServerSocket server;

	public TCPServer() throws IOException {
		super();
		this.server = new ServerSocket(PORT);
	}

	@Override
	public void run() {
		if (state != States.CREATED)
			return;
		state = States.LISTENING;
		dispatchEvent(new Event(Events.SERV_LISTENING_STARTED));
		while (state == States.LISTENING) {
			try {
				Socket socket = server.accept();
				TCPUser user = new TCPUser(socket);
				users.add(user);
				dispatchEvent(new Event(Events.SERV_CLIENT_CONNECTED, user, this));
				pool.submit(user);
			} catch (IOException e) {
				e.printStackTrace();
				dispatchEvent(new Event(Events.SERV_EXCEPTION, e, this));
				terminate();
			}
		}
	}

}
