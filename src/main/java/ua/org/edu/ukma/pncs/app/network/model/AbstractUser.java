package ua.org.edu.ukma.pncs.app.network.model;

import com.google.common.collect.Queues;
import com.google.common.primitives.UnsignedLong;
import lombok.Getter;
import lombok.Setter;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.EventDispatcher;
import ua.org.edu.ukma.pncs.app.event.Events;

import java.util.Deque;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public abstract class AbstractUser extends EventDispatcher implements Runnable {
	@Getter
	@Setter
	protected byte[] key = null;
	public enum States {CREATED, LISTENING, STOPPED}
	@Getter
	protected States state;
	protected int inputCounter;
	protected int outputCounter;

	protected final LinkedBlockingQueue<Packet> packetQueue;
	protected final HashMap<UnsignedLong, Packet> history;

	public AbstractUser() {
		inputCounter = 0;
		outputCounter = 0;
		packetQueue = new LinkedBlockingQueue<>();
		history = new HashMap<>();
	}

	public Packet nextPacket() {
		Packet packet = null;
		try {
			packet = packetQueue.poll(3, TimeUnit.HOURS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (packet == null) ; // todo stop
		return packet;
	}

	public void disconnect() {
		if (state != States.LISTENING && state != States.CREATED)
			return;
		// TODO send disconnection packet
		state = States.STOPPED;
		dispatchEvent(new Event(Events.CLIE_DISCONNECTED, this));
	}

	/**
	 * Method used for processing packets errors or resend requests
	 * Checks packet for an error and processes those cases
	 * @param p
	 * @throws UnsupportedOperationException
	 */
	protected Deque<Packet> processPacketInput(Packet p) throws UnsupportedOperationException {
		Deque<Packet> out = Queues.newArrayDeque();
		if (p.getMessage().getCType().equals(MessageCodes.RESEND_PACKET.ordinal())) {
			// resend packets
			for (UnsignedLong id = UnsignedLong.valueOf(p.getMessage().getMessage());
				 id.compareTo(UnsignedLong.valueOf(outputCounter)) < 0; id = id.plus(UnsignedLong.ONE)) {
				Packet toResend = history.get(id);
				System.out.println("!!=!! Hmm! Resending packet, id "+toResend.getBPktId());
				out.add(toResend);
			}
			return out;
		} else {
			// if packet's ID is bigger than expected - resend
			// if lower - drop
			int compareResult = UnsignedLong.valueOf(inputCounter).compareTo(p.getBPktId());
			if (compareResult < 0)
				throw new UnsupportedOperationException("Corrupted packet: different number, " +
						"missing packets, expected "+(inputCounter)+", got "+p.getBPktId());
			else if (compareResult > 0)
				return null;

			inputCounter++;
			packetQueue.offer(p);
			dispatchEvent(new Event(Events.CLIE_DATA_RECEIVED, p, this));
		}
		return null;
	}

	/**
	 * Method used for requesting for resend a packet
	 */
	protected Packet processPacketError() {
		// request corrupted packets
		// message: lastID
		Packet packet = new Packet((byte)0, UnsignedLong.ZERO,
				new Message(MessageCodes.RESEND_PACKET.ordinal(), 0, String.valueOf(inputCounter)));
		System.out.println("!!=!! Problems! Asking for resending");
//		send(packet, true);
		return packet;
	}

	protected Packet processPacketOutput(Packet p, boolean notEnqueue) {
		if (!notEnqueue) {
			p.setBPktId(UnsignedLong.valueOf(outputCounter));
			history.put(p.getBPktId(), p);
			outputCounter++;
		}
		return p;
	}

	public abstract void send(Packet p);
}
