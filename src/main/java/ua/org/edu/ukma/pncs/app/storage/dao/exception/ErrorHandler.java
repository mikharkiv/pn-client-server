package ua.org.edu.ukma.pncs.app.storage.dao.exception;

import java.sql.SQLException;

public class ErrorHandler {
	public static void handleSQLException(SQLException e) {
		switch (e.getErrorCode()) {
			case SQLExceptionCode.SQLITE_CONSTRAINT_UNIQUE: {
				System.out.println("Such entry already exists");
				break;
			}
			case SQLExceptionCode.SQLITE_ERROR: {
				System.out.println("Wrong SQL query");
				e.printStackTrace();
				break;
			}
			default: {
				System.out.println("Some SQL Exception");
				e.printStackTrace();
				break;
			}
		}
	}
}
