package ua.org.edu.ukma.pncs.gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.scene.Scene;
import ua.org.edu.ukma.pncs.gui.utils.IconManager;

import java.io.IOException;

public class StorageApp extends Application{

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void init() throws Exception {
		super.init();
	}

	@Override
	public void stop() throws Exception {
		super.stop();
	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/Authorization.fxml"));

		IconManager.setIcon(primaryStage);

		primaryStage.setTitle("Storage");
		primaryStage.setResizable(false);

		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getClassLoader().getResource("css/text-field-red-border.css").toExternalForm());

		primaryStage.setOnCloseRequest(event -> {
			Platform.exit();
			System.exit(0);
		});

		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
