package ua.org.edu.ukma.pncs.app.exception;

public class PacketException extends Exception {
	public PacketException(String errorMessage) {
		super(errorMessage);
	}
}
