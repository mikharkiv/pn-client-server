package ua.org.edu.ukma.pncs.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ua.org.edu.ukma.pncs.app.controller.model.AbstractMiddleware;
import ua.org.edu.ukma.pncs.app.controller.model.MiddlewareMessage;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;
import ua.org.edu.ukma.pncs.app.storage.model.User;

public class LoginMiddleware extends AbstractMiddleware {
	private DBService database;
	private boolean loggedIn;

	public LoginMiddleware(DBService database) {
		this.database = database;
		this.loggedIn = false;
	}

	@Override
	protected void handle(MiddlewareMessage p) {
		if (p.getPacket() != null) {
			String[] args = p.getPacket().getMessage().getArgs();
			Integer id = p.getPacket().getMessage().getCType();
			if (id.equals(MessageCodes.LOGIN_DO_LOGIN.ordinal())) {
				String login = args[0];
				String password = args[1];
				User user = database.getUserService().getUser(login, password);
				if (!user.getLogin().isEmpty() && !user.getPassword().isEmpty()) {
					try {
						loggedIn = true;
						User temp = new User(user.getId(), user.getLogin(), null);
						result(new Message(MessageCodes.LOGIN_SUCCESS.ordinal(), 0, new ObjectMapper().writeValueAsString(temp)));
					} catch (JsonProcessingException e) {
						result(new Message(MessageCodes.LOGIN_FAIL.ordinal(), 0, ""));
					}
				} else {
					result(new Message(MessageCodes.LOGIN_FAIL.ordinal(), 0, ""));
				}
			} else if (id.equals(MessageCodes.LOGIN_DO_LOGOUT.ordinal())) {
				loggedIn = false;
			} else {
				// pass packet if logged
				if (loggedIn)
					pass(p);
				else
					result(new Message(MessageCodes.LOGIN_NOT_LOGGED.ordinal(), 0, ""));
			}
		} else if (p.getEvent() != null) {
			if (p.getEvent().getEventType() == Events.CLIE_DISCONNECTED)
				loggedIn = false;
			else
				pass(p);
		}
	}
}
