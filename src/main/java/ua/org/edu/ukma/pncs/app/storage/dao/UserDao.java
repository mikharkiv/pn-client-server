package ua.org.edu.ukma.pncs.app.storage.dao;

import ua.org.edu.ukma.pncs.app.storage.dao.exception.ErrorHandler;
import ua.org.edu.ukma.pncs.app.storage.dao.exception.TransactionException;
import ua.org.edu.ukma.pncs.app.storage.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDao implements Dao<User> {
	public static final String TABLE_NAME = "Users";

	public static final String COLUMN_1_ID = "user_id";
	public static final String COLUMN_2_LOGIN = "user_login";
	public static final String COLUMN_3_PASSWORD = "user_password";

	public static final String CREATE_TABLE = "create table if not exists " + TABLE_NAME + " ( '" +
		COLUMN_1_ID + "' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '" +
		COLUMN_2_LOGIN + "' TEXT NOT NULL UNIQUE, '" +
		COLUMN_3_PASSWORD + "' TEXT NOT NULL " +
		");";

	public static final String INSERT_USER = "INSERT INTO " + TABLE_NAME + "(" + COLUMN_2_LOGIN + ", " + COLUMN_3_PASSWORD + ") VALUES (?,?)";
	public static final String SELECT_USERS = "SELECT * FROM " + TABLE_NAME;
	public static final String DELETE_USER = "DELETE FROM " + TABLE_NAME + " WHERE " + COLUMN_2_LOGIN + "=? AND " + COLUMN_3_PASSWORD + "=?";

	public static final String GET_USER = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_2_LOGIN + "=? AND " + COLUMN_3_PASSWORD + "=?";
	public static final String GET_BY_ID = "SELECT * FROM " + TABLE_NAME + "  WHERE " + COLUMN_1_ID + "=?";

	public static final String UPDATE = "UPDATE " + TABLE_NAME + " SET " + COLUMN_2_LOGIN + "=? AND " + COLUMN_3_PASSWORD + "=? WHERE" + COLUMN_2_LOGIN + "=? AND " + COLUMN_3_PASSWORD + "=?";
	public static final String UPDATE_BY_ID = "UPDATE " + TABLE_NAME + " SET " + COLUMN_2_LOGIN + "=?, " + COLUMN_3_PASSWORD + "=? WHERE " + COLUMN_1_ID + "=?";

	private final Connection connection;

	public UserDao(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @param id
	 * @return User from the table by id
	 */
	@Override
	public Optional<User> get(int id) {
		try {
			PreparedStatement st = connection.prepareStatement(GET_BY_ID);
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			User resultUser = null;
			if (res.next()) {
				resultUser = new User(res.getInt(COLUMN_1_ID), res.getString(COLUMN_2_LOGIN), res.getString(COLUMN_3_PASSWORD));
			}
			res.close();
			st.close();
			return Optional.ofNullable(resultUser);
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			return Optional.empty();
		}
	}

	/**
	 * @return All users from the table
	 */
	@Override
	public List<User> getAll() {
		try {
			PreparedStatement st = connection.prepareStatement(SELECT_USERS);

			ResultSet res = st.executeQuery();
			ArrayList<User> resultList = new ArrayList<>();
			while (res.next()) {
				resultList.add(new User(res.getInt(COLUMN_1_ID), res.getString(COLUMN_2_LOGIN), res.getString(COLUMN_3_PASSWORD)));
			}
			res.close();
			st.close();
			return resultList;
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			return null;
		}
	}

	/**
	 * Save user
	 *
	 * @param user
	 * @throws TransactionException
	 */
	@Override
	public void save(User user) throws TransactionException {
		try {
			PreparedStatement statement = connection.prepareStatement(INSERT_USER);
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			int result = statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * Update existing user in the table
	 *
	 * @param user
	 * @param params
	 * @throws TransactionException
	 */
	@Override
	public void update(User user, Object... params) throws TransactionException {
		try {
			if (params.length < 1) return;
			PreparedStatement statement = connection.prepareStatement(UPDATE);
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			statement.setString(3, params[0] == null ? user.getLogin() : (String) params[0]);
			if (params.length >= 2)
				statement.setString(2, params[1] == null ? user.getPassword() : (String) params[1]);
			else statement.setString(2, user.getPassword());
			int result = statement.executeUpdate();
			statement.close();
			if (result < 0) throw new TransactionException("Transaction failed");
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	public void updateUserPlease(User user) throws TransactionException {

		System.out.println("ОСЬ ЮЗЕР: " + user.getLogin() + "    " + user.getPassword() + "   " + user.getId());

		try {
			PreparedStatement statement = connection.prepareStatement(UPDATE_BY_ID);
			statement.setInt(3, user.getId());
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			int result = statement.executeUpdate();
			statement.close();
			if (result < 0) throw new TransactionException("Transaction failed");
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * Delete user from the table
	 *
	 * @param user
	 * @throws TransactionException
	 */
	@Override
	public void delete(User user) throws TransactionException {
		try {
			PreparedStatement statement = connection.prepareStatement(DELETE_USER);
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			int result = statement.executeUpdate();
			if (result <= 0) throw new TransactionException("User has not been deleted");
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * Clear the table
	 *
	 * @throws TransactionException
	 */
	@Override
	public void clear() throws TransactionException {
		try {
			Statement statement = connection.createStatement();
			int result = statement.executeUpdate("delete from " + TABLE_NAME);
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 *
	 * @param login
	 * @param password
	 * @return User from the table by login and password
	 */
	public Optional<User> get(String login, String password) {
		try {
			PreparedStatement st = connection.prepareStatement(GET_USER);
			st.setString(1, login);
			st.setString(2, password);
			ResultSet res = st.executeQuery();
			User resultUser = null;
			if (res.next()) {
				resultUser = new User(res.getInt(COLUMN_1_ID), res.getString(COLUMN_2_LOGIN), res.getString(COLUMN_3_PASSWORD));
			}
			res.close();
			st.close();
			return Optional.ofNullable(resultUser);
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			return Optional.empty();
		}
	}
}
