package ua.org.edu.ukma.pncs.app.network.tcp;

import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.exception.PacketValidatingException;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.model.AbstractUser;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.Deque;

public class TCPUser extends AbstractUser implements Runnable {
	private Socket s;
	private InputStream is;
	private OutputStream os;
	private InetAddress addr;
	private int port;

	public static final int RECONNECT_SECONDS_TIMEOUT = 5;
	public static final int RECONNECT_TRY_COUNT = 4;

	public TCPUser(Socket socket) {
		super();
		this.s = socket;
		init();
	}

	private void init() {
		try {
			this.is = s.getInputStream();
			this.os = s.getOutputStream();
			this.addr = s.getInetAddress();
			this.port = s.getPort();
			this.state = States.CREATED;
		} catch (IOException e) {
			this.state = States.STOPPED;
			dispatchEvent(new Event(Events.CLIE_EXCEPTION, e, this));
		}
	}

	@Override
	public void run() {
		if (state != States.CREATED)
			return;
		state = States.LISTENING;
		dispatchEvent(new Event(Events.CLIE_LISTENING_STARTED, this));
		while (state == States.LISTENING) {
			read();
		}
	}

	private synchronized void read() {
		int s_byte = -1;
		try {
			if (!s.isConnected()) throw new IOException();
			s_byte = is.read();
			if (s_byte >= 0) {
				// TODO maybe refactor this code later
				byte[] headerWithCrc = new byte[Packet.HEADER_SIZE_NO_CRC + Short.BYTES];
				headerWithCrc[0] = (byte) s_byte;
				int readCount = is.read(headerWithCrc, 1, headerWithCrc.length - 1);
				if (readCount != headerWithCrc.length - 1)
					throw new UnsupportedOperationException("Corrupted packet: header underflow");
				ByteBuffer buff = ByteBuffer.wrap(headerWithCrc);
				buff.get();
				buff.get();
				buff.getLong();
				int packLen = buff.getInt();
				int msgAndCrcLen = packLen - Packet.PACKET_SIZE_NO_MESSAGE;
				byte[] messageAndCrc = new byte[msgAndCrcLen + Short.BYTES];
				int read = is.read(messageAndCrc);
				if (read != messageAndCrc.length)
					throw new UnsupportedOperationException("Corrupted packet: message and CRC underflow");
				byte[] packet = new byte[headerWithCrc.length + messageAndCrc.length];

				System.arraycopy(headerWithCrc, 0, packet, 0, headerWithCrc.length);
				System.arraycopy(messageAndCrc, 0, packet, headerWithCrc.length, messageAndCrc.length);
				Packet p = new Packet(key, packet);

				Deque<Packet> temp = processPacketInput(p);
				if (temp != null && !temp.isEmpty())
					for (Packet packet1 : temp)
						send(packet1, true);
			} else {
				// disconnected
				throw new IOException("Client disconnected");
			}
		} catch (UnsupportedOperationException | GeneralSecurityException | PacketValidatingException e) {
			send(processPacketError(), true);
		} catch (IOException e) {
			// reconnecting if connection lost
			tryReconnect();
		}
	}

	private void send(Packet p,  boolean notEnqueue) {
		if (state != States.LISTENING)
			return;
		try {
			p = processPacketOutput(p, notEnqueue);
			System.out.println(p);
			os.write(p.toByteArray(key));
			os.flush();
		} catch (IOException | GeneralSecurityException e) {
			tryReconnect();
		}
	}

	private void tryReconnect() {
		if (state == States.STOPPED) return;
		System.out.println("Connection lost. Trying reconnecting");

		boolean connected = false;
		for (int i = 0; i < RECONNECT_TRY_COUNT; ++i) {
			try {
				System.out.printf("Trying reconnect. Attempt %d \n", i);
				Thread.sleep(RECONNECT_SECONDS_TIMEOUT * 1000);
				s = new Socket(addr, port);
				is = s.getInputStream();
				os = s.getOutputStream();
				connected = true;
				break;
			} catch (IOException | InterruptedException ioException) {
				System.out.println("Attempt failed");
			}
		}

		if (connected) {
			System.out.println("Reconnecting successful");
		} else {
			System.out.println("Reconnecting failed");
			disconnect();
		}
	}

	public void send(Packet p) {
		send(p, false);
	}

	@Override
	public void disconnect() {
		super.disconnect();
		try {
			s.close();
			is.close();
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
