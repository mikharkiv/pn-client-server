package ua.org.edu.ukma.pncs.gui.utils;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

import java.util.Collections;

public class Validator {

	public static final String GREEN_COLOR = "#00ff00";
	public static final String RED_COLOR = "#ff0000";

	public static final String NOT_NUMBER_PATTERN = "[^\\d]";
	public static final String INTEGER_PATTERN = "\\d*";
	public static final String DOUBLE_PATTERN = "(\\d*|\\d+\\,\\d*)";

	public static void setUpTextValidation(final TextField tf, final Label msg) {
		tf.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable,
								String oldValue, String newValue) {
				validate(tf, msg);
			}

		});
		validate(tf, msg);
	}

	public static void setUpPasswordValidation(final PasswordField pf, final Label msg) {
		pf.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable,
								String oldValue, String newValue) {
				validatePassword(pf, msg);
			}

		});
		validatePassword(pf, msg);
	}

	public static void setUpIntegerValidation(final TextField tf) {
		validateTextField(tf, INTEGER_PATTERN);
	}

	public static void setUpIntegerValidation(final Spinner spinner) {
		TextField tf = spinner.getEditor();
		validateTextField(tf, INTEGER_PATTERN);
	}

	public static void setUpDoubleValidation(final Spinner spinner) {
		TextField tf = spinner.getEditor();
		validateTextField(tf, DOUBLE_PATTERN);
	}

	private static void validateTextField(final TextField tf, String pattern) {
		tf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue,
								String newValue) {
				if (pattern == DOUBLE_PATTERN) {
					try {
						Double.parseDouble(newValue);
					} catch (NumberFormatException e) {
						tf.setText(oldValue);
					}
				} else if (pattern == INTEGER_PATTERN) {
					try {
						Integer.parseInt(newValue);
					} catch (NumberFormatException e) {
						tf.setText(oldValue);
					}
				} else if (newValue.matches("")) {
					tf.setText("1");
				}
			}
		});
	}

	private static void validate(TextField tf, Label msg) {
		ObservableList<String> styleClass = tf.getStyleClass();
		if (tf.getText().trim().length() == 0) {
			msg.setTextFill(Color.web(RED_COLOR));
			msg.setText("Empty Field !");
			msg.setVisible(true);
			if (!styleClass.contains("error")) {
				styleClass.add("error");
			}
		} else {
			// remove all occurrences:
			msg.setVisible(false);
			styleClass.removeAll(Collections.singleton("error"));
		}
	}

	private static void validatePassword(PasswordField pf, Label msg) {
		ObservableList<String> styleClass = pf.getStyleClass();
		if (pf.getText().trim().length() < 4) {
			msg.setTextFill(Color.web(RED_COLOR));
			msg.setText("Too Short");
			msg.setVisible(true);
			if (!styleClass.contains("error")) {
				styleClass.add("error");
			}
		} else {
			// remove all occurrences:
			msg.setVisible(false);
			styleClass.removeAll(Collections.singleton("error"));
		}
	}

}
