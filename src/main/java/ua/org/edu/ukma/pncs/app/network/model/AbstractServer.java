package ua.org.edu.ukma.pncs.app.network.model;

import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.EventDispatcher;
import ua.org.edu.ukma.pncs.app.event.Events;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class AbstractServer extends EventDispatcher implements Runnable {
	public static final int PORT = 3956;
	public enum States { CREATED, LISTENING, STOPPED }

	protected States state;
	protected final ArrayList<AbstractUser> users;
	protected final ExecutorService pool;

	public AbstractServer() {
		this.state = States.CREATED;
		this.users = new ArrayList<>();
		this.pool = Executors.newCachedThreadPool();
	}

	public void terminate() {
		disconnectAll();
		state = States.STOPPED;
		dispatchEvent(new Event(Events.SERV_STOPPED, this));
	}

	public void broadcast(Packet p) {
		for (AbstractUser user : users)
			user.send(p);
	}

	public void disconnect(AbstractUser u) {
		u.disconnect();
	}

	public void disconnectAll() {
		new ArrayList<>(users).forEach(user -> {
			disconnect(user);
			users.remove(user);
		});
	}
}
