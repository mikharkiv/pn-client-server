package ua.org.edu.ukma.pncs.app.client;

import com.google.common.primitives.UnsignedLong;
import lombok.Getter;
import lombok.Setter;
import ua.org.edu.ukma.pncs.app.network.entity.KeyExchangeMessage;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.client.controller.KeyExchange;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.EventListener;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.model.AbstractUser;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public abstract class AbstractClient implements EventListener {
	@Getter
	@Setter
	protected AbstractUser user;
	@Getter
	@Setter
	protected KeyExchange keyExchange;

	@Override
	public void onEvent(Event e) {
		if (e.getEventType() == Events.CLIE_DATA_RECEIVED) {
			Packet packet = (Packet) e.getData()[0];
			MessageCodes msgType = MessageCodes.values()[packet.getMessage().getCType()];
			System.out.println(packet);

			if (keyExchange.getState() == KeyExchange.State.WAITING_FOR_PUBLIC_KEY && msgType == MessageCodes.KEY_EXCHANGE) {
				try {
					byte[] serverPubKey = ((KeyExchangeMessage) packet.getMessage()).getKey();
					keyExchange.processPublicKey(serverPubKey);
					user.send(new Packet((byte) 0, UnsignedLong.ZERO,
						new KeyExchangeMessage(MessageCodes.KEY_EXCHANGE.ordinal(), 0, keyExchange.getPublicKey())));
					user.setKey(keyExchange.getSharedSecret());
				} catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidAlgorithmParameterException | InvalidKeyException ex) {
					ex.printStackTrace();
				}
			} else if (keyExchange.getState() == KeyExchange.State.WAITING_FOR_APPROVAL && msgType == MessageCodes.CONNECTION_ESTABLISHED) {
				keyExchange.approveConnection(packet.getMessage());
			}

		}
	}
}
