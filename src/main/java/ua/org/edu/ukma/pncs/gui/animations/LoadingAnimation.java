package ua.org.edu.ukma.pncs.gui.animations;

import animatefx.animation.AnimationFX;
import animatefx.animation.Bounce;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.util.Duration;

import java.util.ArrayList;

public class LoadingAnimation {

	private final double speed = 0.5;
	private final int cycleDuration = 100;
	private final int cycleCount = 100;

	private final int delayDurationDiff = 100;

	private int delayDuration = 500;

	private ObservableList<Node> loadingGroup;
	private ArrayList<AnimationFX> animations;

	public LoadingAnimation(Group loadingGroup) {
		this.loadingGroup = loadingGroup.getChildren();

		String duration = Integer.toString(delayDuration) + "ms";

		animations = new ArrayList<>();

		for (Node node : this.loadingGroup) {
			System.out.println(duration);
			animations.add(new Bounce(node).setSpeed(speed).setCycleDuration(cycleDuration).setCycleCount(cycleCount).setDelay(Duration.valueOf(duration)));
			delayDuration += delayDurationDiff;
			duration = Integer.toString(delayDuration) + "ms";
		}
	}

	public void loadingAnimationStart() {
		for (Node node : loadingGroup) {
			node.setVisible(true);
		}
		for (AnimationFX anim : animations) {
			anim.play();
		}
	}

	public void loadingAnimationStop() {
		for (Node node : loadingGroup) {
			node.setVisible(false);
		}
		for (AnimationFX anim : animations) {
			anim.stop();
		}
	}

}
