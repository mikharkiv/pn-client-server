package ua.org.edu.ukma.pncs.gui.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ua.org.edu.ukma.pncs.gui.animations.LoadingAnimation;
import ua.org.edu.ukma.pncs.gui.utils.AlertPane;
import ua.org.edu.ukma.pncs.gui.utils.Validator;
import ua.org.edu.ukma.pncs.app.client.ClientNetworkFacade;
import ua.org.edu.ukma.pncs.app.storage.model.Product;
import ua.org.edu.ukma.pncs.app.storage.model.Utils;

public class PlustSubtractDialogController {

	@FXML
	public Label productQuantityLabel;
	@FXML
	public TextField quantityField;
	@FXML
	public Button plus;
	@FXML
	public Button minus;
	@FXML
	public Button cancel;
	@FXML
	public Label productNameLabel;

	Product product = Utils.emptyProduct();

	@FXML
	public javafx.scene.Group loadingGroup;

	LoadingAnimation loadingAnimation;

	volatile private static Product editProduct;

	private static boolean isError = false;

	private static String serverError = "error";

	@FXML
	public void initialize() {
		loadingAnimation = new LoadingAnimation(loadingGroup);
		update();
		Validator.setUpIntegerValidation(quantityField);
	}

	public void onPlusAction(ActionEvent actionEvent) {
		new Thread(() -> {
			Platform.runLater(() -> {
				loadingStart();
			});
			tryToPlus();
			Platform.runLater(() -> {
				loadingStop();
				if (isError) {
					isError = false;
					AlertPane.showError("Server error", serverError);
				} else if (editProduct == null)
					AlertPane.showError("Server error", "No such product");
				else {
					product = editProduct;
					update();
				}
			});

		}).start();
	}

	public void onMinusAction(ActionEvent actionEvent) {
		new Thread(() -> {
			Platform.runLater(() -> {
				loadingStart();
			});
			tryToSubtract();
			Platform.runLater(() -> {
				loadingStop();
				if (editProduct == null)
					AlertPane.showError("Server error", "No such product");
				else {
					product = editProduct;
					update();
				}

			});
		}).start();
	}

	private void tryToPlus() {
		product.add(Integer.parseInt(quantityField.getText()));
		ClientNetworkFacade.updateProduct(product);

		int timeout = 0;

		while (editProduct == null) {
			try {
				Thread.sleep(100);
				timeout += 100;
				if (timeout > 5000) return;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void tryToSubtract() {
		product.remove(Integer.parseInt(quantityField.getText()));
		ClientNetworkFacade.updateProduct(product);

		int timeout = 0;

		while (editProduct == null) {
			try {
				Thread.sleep(100);
				timeout += 100;
				if (timeout > 5000) return;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void onCancelAction(ActionEvent actionEvent) {
		closeStage(actionEvent);
	}

	private void loadingStart() {
		quantityField.setEditable(false);
		plus.setVisible(false);
		minus.setVisible(false);
		cancel.setVisible(false);
		loadingAnimation.loadingAnimationStart();
	}

	private void loadingStop() {
		loadingAnimation.loadingAnimationStop();
		quantityField.setEditable(true);
		plus.setVisible(true);
		minus.setVisible(true);
		cancel.setVisible(true);
	}

	private void closeStage(ActionEvent event) {
		Node source = (Node) event.getSource();
		Stage stage = (Stage) source.getScene().getWindow();
		stage.close();
	}

	public void loadProduct(Product product) {
		this.product = product;
		update();
	}

	private void update() {
		editProduct = null;
		productNameLabel.setText(product.getName());
		productQuantityLabel.setText("Quantity: " + product.getQuantity().toString());
	}

	public static void receiveProduct(Product prod) {
		PlustSubtractDialogController.editProduct = prod;
	}

	public static void serverError(String error) {
		isError = true;
		serverError = error;
	}
}
