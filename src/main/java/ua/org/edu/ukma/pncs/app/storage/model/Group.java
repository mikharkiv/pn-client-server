package ua.org.edu.ukma.pncs.app.storage.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Group {
	private Integer id;
	@EqualsAndHashCode.Include
	private String name;
	private String description;

	public Group(String name) {
		this.name = name;
		id = -1;
		description = "";
	}

	public Group(String name, String description) {
		this.name = name;
		this.description = description;
		id = -1;
	}

	public String info(){
		String info = "Id: " + id + "\n\n" +
			"Description: " + description;
		return info;
	}

	@Override
	public String toString(){
		return name;
	}
}
