package ua.org.edu.ukma.pncs.app.storage.dao.exception;

public class SQLExceptionCode {
	public static final int SQLITE_ERROR = 1;
	public static final int SQLITE_CONSTRAINT_UNIQUE = 19;
}
