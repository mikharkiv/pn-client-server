package ua.org.edu.ukma.pncs.app;

import ua.org.edu.ukma.pncs.app.controller.MiddlewareController;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.EventListener;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.model.AbstractServer;
import ua.org.edu.ukma.pncs.app.network.model.AbstractUser;
import ua.org.edu.ukma.pncs.app.network.tcp.TCPServer;
import ua.org.edu.ukma.pncs.app.network.udp.UDPServer;
import ua.org.edu.ukma.pncs.app.storage.db.DBHelper;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;

import java.io.IOException;
import java.sql.Connection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerRunner implements EventListener {
	private final AbstractServer server;
	private final ExecutorService pool;
	private final DBService database;

	public ServerRunner(boolean tcp) throws IOException {
		Connection connection = DBHelper.getConnection();
		database = DBService.instantiateService(connection);
		server = tcp ? new TCPServer() : new UDPServer();
		pool = Executors.newCachedThreadPool();
		init();
	}

	private void init() {
		server.addEventListener(this);
	}

	public void run() {
		new Thread(server).start();
		System.out.println("Server started");
	}

	public void terminateConnections() {
		server.terminate();
	}

	public void terminate() {
		terminateConnections();
		DBHelper.closeConnection();
		pool.shutdown();
	}

	@Override
	public void onEvent(Event e) {
		if (e.getEventType() == Events.SERV_CLIENT_CONNECTED) {
			MiddlewareController controller = new MiddlewareController((AbstractUser) e.getData()[0], database);
			pool.submit(controller);
		}
	}
}
