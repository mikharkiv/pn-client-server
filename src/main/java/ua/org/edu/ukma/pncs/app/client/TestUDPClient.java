package ua.org.edu.ukma.pncs.app.client;

import com.google.common.primitives.UnsignedLong;
import javafx.util.Pair;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import ua.org.edu.ukma.pncs.app.network.entity.KeyExchangeMessage;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.client.controller.KeyExchange;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.EventDispatcher;
import ua.org.edu.ukma.pncs.app.event.EventListener;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.model.AbstractUser;
import ua.org.edu.ukma.pncs.app.network.tcp.TCPServer;
import ua.org.edu.ukma.pncs.app.network.udp.UDPUser;

import java.net.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

@EqualsAndHashCode()
public class TestUDPClient extends EventDispatcher implements EventListener, Runnable {
	private enum State {INIT, CONNECTION, KEY_EXCHANGE, RUNNING}

	@Getter
	@Setter
	protected AbstractUser user;
	@Getter
	@Setter
	protected KeyExchange keyExchange;
	volatile private State state;
	private DatagramSocket s;
	private LinkedBlockingQueue<Packet> packetsToSend;
	private int sent;
	private int received;

	private static final List<Pair<MessageCodes, String>> testMessages = Arrays.asList(
		new Pair<>(MessageCodes.ADD_PRODUCTS, "Fruit$Apples$20"),
		new Pair<>(MessageCodes.SUBTRACT_PRODUCTS, "Vegetables$Tomatoes$10"),
		new Pair<>(MessageCodes.HOW_MANY, "Fruit$Apples"),
		new Pair<>(MessageCodes.HOW_MANY, "Vegetables$Tomatoes"));

	public TestUDPClient(){
		this(testMessages);
	}

	public TestUDPClient(List<Pair<MessageCodes, String>> messages) {
		try {
			s = new DatagramSocket();
			setUser(new UDPUser(s, InetAddress.getByName("localhost"), TCPServer.PORT, UDPUser.Role.CLIENT));
			KeyExchange keyExchange = new KeyExchange();
			keyExchange.addEventListener(this);
			setKeyExchange(keyExchange);
			getUser().addEventListener(this);
			packetsToSend = new LinkedBlockingQueue<>();
			sent = 0;
			received = -2;
			fillTestData(messages);
			state = State.INIT;
		} catch (UnknownHostException | SocketException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		new Thread(getUser()).start();
		state = State.CONNECTION;

		System.out.println("Starting client");
		while (sent > received) {
			getUser().nextPacket();
			System.out.println("Got response");
			received++;
		}
		System.out.println("Everything finished successfully, client execution done\n");
		getUser().disconnect();
		System.out.println("Disconnected");
	}

	private void fillTestData(List<Pair<MessageCodes, String>> messages) {
		for (Pair<MessageCodes, String> pair : messages) {
			fillTestPacket(new Packet((byte) 0, UnsignedLong.ZERO,
				new Message(pair.getKey().ordinal(), 0, pair.getValue())));
		}
		System.out.println("Data sent. Waiting for response");
	}

	private void fillTestPacket(Packet p) {
		packetsToSend.offer(p);
		sent++;
	}

	private void establishConnection() {
		getUser().send(new Packet((byte) 0, UnsignedLong.ZERO,
				new Message(0, 0, "connect")));

		state = State.KEY_EXCHANGE;
		System.out.println("Client: waiting for key exchange");
	}

	private void sendData() {
		state = State.RUNNING;
		while (!packetsToSend.isEmpty()) {
			try {
				getUser().send(packetsToSend.take());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Client: Done sending packets");
	}

	@Override
	public void onEvent(Event e) {
		if (e.getEventType() == Events.CLIE_DATA_RECEIVED) {
			Packet packet = (Packet) e.getData()[0];
			MessageCodes msgType = MessageCodes.values()[packet.getMessage().getCType()];
			System.out.println(packet);

			if (keyExchange.getState() == KeyExchange.State.WAITING_FOR_PUBLIC_KEY && msgType == MessageCodes.KEY_EXCHANGE) {
				try {
					byte[] serverPubKey = ((KeyExchangeMessage) packet.getMessage()).getKey();
					keyExchange.processPublicKey(serverPubKey);
					user.send(new Packet((byte) 0, UnsignedLong.ZERO,
							new KeyExchangeMessage(MessageCodes.KEY_EXCHANGE.ordinal(), 0, keyExchange.getPublicKey())));
					user.setKey(keyExchange.getSharedSecret());
				} catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidAlgorithmParameterException | InvalidKeyException ex) {
					ex.printStackTrace();
				}
			} else if (keyExchange.getState() == KeyExchange.State.WAITING_FOR_APPROVAL && msgType == MessageCodes.CONNECTION_ESTABLISHED) {
				keyExchange.approveConnection(packet.getMessage());
			}

		} else if (e.getEventType() == Events.CLIE_LISTENING_STARTED) {
			establishConnection();
		} else if (e.getEventType() == Events.KEY_EXCHANGE_CONNECTION_ESTABLISHED) {
			sendData();
		}
	}

}
