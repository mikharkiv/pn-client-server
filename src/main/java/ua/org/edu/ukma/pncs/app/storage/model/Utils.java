package ua.org.edu.ukma.pncs.app.storage.model;

public class Utils {
	public static Product emptyProduct() {
		return new Product(emptyGroup(), "", 0, 0d);
	}

	public static Group emptyGroup() {
		return new Group("");
	}

	public static User emptyUser() {
		return new User("","");
	}
}
