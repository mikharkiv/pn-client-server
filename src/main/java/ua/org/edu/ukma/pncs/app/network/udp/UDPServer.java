package ua.org.edu.ukma.pncs.app.network.udp;

import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.model.AbstractServer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;

public class UDPServer extends AbstractServer {
	private final DatagramSocket server;
	private final HashMap<String, UDPUser> connections;

	public UDPServer() throws IOException {
		super();
		this.server = new DatagramSocket(PORT);
		connections = new HashMap<>();
	}

	@Override
	public void run() {
		if (state != States.CREATED)
			return;
		state = States.LISTENING;
		dispatchEvent(new Event(Events.SERV_LISTENING_STARTED));
		while (state == States.LISTENING) {
			try {
				DatagramPacket packet = new DatagramPacket(new byte[Packet.MAX_PACKET_SIZE], Packet.MAX_PACKET_SIZE);
				server.receive(packet);

				InetAddress address = packet.getAddress();
				int port = packet.getPort();

				String key = address.getHostAddress().concat(String.valueOf(port));
				UDPUser user = connections.get(key);
				if (user == null) {
					user = new UDPUser(server, address, port, UDPUser.Role.SERVER);
					users.add(user);
					connections.put(key, user);
					dispatchEvent(new Event(Events.SERV_CLIENT_CONNECTED, user, this));
					pool.submit(user);
				} else {
					user.putPacketToProcess(packet);
				}
			} catch (IOException e) {
				e.printStackTrace();
				dispatchEvent(new Event(Events.SERV_EXCEPTION, e, this));
				terminate();
			}
		}
	}

}
