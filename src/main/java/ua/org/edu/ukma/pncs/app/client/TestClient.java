package ua.org.edu.ukma.pncs.app.client;

import com.google.common.collect.Queues;
import com.google.common.primitives.UnsignedLong;
import javafx.util.Pair;
import lombok.Getter;
import lombok.Setter;
import ua.org.edu.ukma.pncs.app.network.entity.KeyExchangeMessage;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.client.controller.KeyExchange;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.EventDispatcher;
import ua.org.edu.ukma.pncs.app.event.EventListener;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.model.AbstractUser;
import ua.org.edu.ukma.pncs.app.network.tcp.TCPServer;
import ua.org.edu.ukma.pncs.app.network.tcp.TCPUser;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

public class TestClient extends EventDispatcher implements EventListener, Runnable {

	@Getter
	@Setter
	protected AbstractUser user;
	@Getter
	@Setter
	protected KeyExchange keyExchange;

	private Deque<Packet> packetsToSend;
	private int sent;
	private int received;

	private static final List<Pair<MessageCodes, String>> testMessages = Arrays.asList(
		new Pair<>(MessageCodes.ADD_PRODUCTS, "Fruit$Apples$20"),
		new Pair<>(MessageCodes.SUBTRACT_PRODUCTS, "Vegetables$Tomatoes$10"),
		new Pair<>(MessageCodes.HOW_MANY, "Fruit$Apples"),
		new Pair<>(MessageCodes.HOW_MANY, "Vegetables$Tomatoes"));

	public TestClient(){
		this(testMessages);
	}

	public TestClient(List<Pair<MessageCodes, String>> messages) {
		Socket s = new Socket();
		try {
			s.connect(new InetSocketAddress("127.0.0.1", TCPServer.PORT));
			setUser(new TCPUser(s));
			KeyExchange keyExchange = new KeyExchange();
			keyExchange.addEventListener(this);
			setKeyExchange(keyExchange);
			getUser().addEventListener(this);
			packetsToSend = Queues.newArrayDeque();
			sent = 0;
			received = -2;
			fillTestData(messages);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void fillTestData(List<Pair<MessageCodes, String>> messages) {
		for (Pair<MessageCodes, String> pair : messages) {
			fillTestPacket(new Packet((byte) 0, UnsignedLong.ZERO,
				new Message(pair.getKey().ordinal(), 0, pair.getValue())));
		}
		System.out.println("Data sent. Waiting for response");
	}

	private void fillTestPacket(Packet p) {
		packetsToSend.offer(p);
		sent++;
	}

	private void sendData() {
		while (!packetsToSend.isEmpty())
			getUser().send(packetsToSend.poll());
	}

	@Override
	public void run() {
		new Thread(getUser()).start();

		System.out.println("Starting client");
		while (sent > received) {
			getUser().nextPacket();
			System.out.println("Got response");
			received++;
		}
		System.out.println("Everything finished successfully, client execution done\n");
		getUser().disconnect();
		System.out.println("Disconnected");
	}

	@Override
	public void onEvent(Event e) {
		if (e.getEventType() == Events.CLIE_DATA_RECEIVED) {
			Packet packet = (Packet) e.getData()[0];
			MessageCodes msgType = MessageCodes.values()[packet.getMessage().getCType()];
			System.out.println(packet);

			if (keyExchange.getState() == KeyExchange.State.WAITING_FOR_PUBLIC_KEY && msgType == MessageCodes.KEY_EXCHANGE) {
				try {
					byte[] serverPubKey = ((KeyExchangeMessage) packet.getMessage()).getKey();
					keyExchange.processPublicKey(serverPubKey);
					user.send(new Packet((byte) 0, UnsignedLong.ZERO,
						new KeyExchangeMessage(MessageCodes.KEY_EXCHANGE.ordinal(), 0, keyExchange.getPublicKey())));
					user.setKey(keyExchange.getSharedSecret());
				} catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidAlgorithmParameterException | InvalidKeyException ex) {
					ex.printStackTrace();
				}
			} else if (keyExchange.getState() == KeyExchange.State.WAITING_FOR_APPROVAL && msgType == MessageCodes.CONNECTION_ESTABLISHED) {
				keyExchange.approveConnection(packet.getMessage());
			}
		} else if (e.getEventType() == Events.KEY_EXCHANGE_CONNECTION_ESTABLISHED) {
			sendData();
		}
	}
}
