package ua.org.edu.ukma.pncs.app.event;

public interface EventListener {
	void onEvent(Event e);
}
