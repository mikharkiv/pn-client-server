package ua.org.edu.ukma.pncs.app.client.controller;

import lombok.Getter;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.controller.KeyExchangeMiddleware;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.EventDispatcher;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.util.ArrayUtils;

import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class KeyExchange extends EventDispatcher {
	public enum State {WAITING_FOR_PUBLIC_KEY, WAITING_FOR_APPROVAL, CONNECTION_ESTABLISHED}

	@Getter
	volatile private State state;
	private byte[] clientSharedSecret;
	private byte[] clientPubKeyEnc;

	public final static String APPROVE_KEY_WORD = "Success";

	public KeyExchange() {
		state = State.WAITING_FOR_PUBLIC_KEY;
		clientSharedSecret = null;
	}

	public byte[] getSharedSecret() {
		return state == State.WAITING_FOR_PUBLIC_KEY ? null : clientSharedSecret;
	}

	public byte[] getPublicKey() {
		return state == State.WAITING_FOR_PUBLIC_KEY ? null : clientPubKeyEnc;
	}

	public void approveConnection(Message msg) {
		if (msg.getMessage().equals(APPROVE_KEY_WORD)) {
			System.out.println("Connection successfully established");
			this.state = State.CONNECTION_ESTABLISHED;
			dispatchEvent(new Event(Events.KEY_EXCHANGE_CONNECTION_ESTABLISHED));
		} else {
			System.out.println("Connection NOT established");
		}
	}

	public void processPublicKey(byte[] pubKeyEnc) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException, InvalidKeyException {
		final KeyFactory clientKeyFac = KeyFactory.getInstance(KeyExchangeMiddleware.KEY_ALGORITHM);
		final X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(pubKeyEnc);
		final PublicKey serverPubKey = clientKeyFac.generatePublic(x509KeySpec);
		final DHParameterSpec dhParamFromServerPubKey = ((DHPublicKey) serverPubKey).getParams();
		final KeyPairGenerator clientKeyPairGen = KeyPairGenerator.getInstance(KeyExchangeMiddleware.KEY_ALGORITHM);
		clientKeyPairGen.initialize(dhParamFromServerPubKey);
		final KeyPair clientKeyPair = clientKeyPairGen.generateKeyPair();
		final KeyAgreement clientKeyAgree = KeyAgreement.getInstance(KeyExchangeMiddleware.KEY_ALGORITHM);
		clientKeyAgree.init(clientKeyPair.getPrivate());

		clientPubKeyEnc = clientKeyPair.getPublic().getEncoded();
		clientKeyAgree.doPhase(serverPubKey, true);
		clientSharedSecret = ArrayUtils.shortenSecretKey(clientKeyAgree.generateSecret());
		state = State.WAITING_FOR_APPROVAL;
	}

}
