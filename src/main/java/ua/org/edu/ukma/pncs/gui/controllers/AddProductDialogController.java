package ua.org.edu.ukma.pncs.gui.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ua.org.edu.ukma.pncs.gui.animations.LoadingAnimation;
import ua.org.edu.ukma.pncs.gui.manager.ProductTable;
import ua.org.edu.ukma.pncs.gui.utils.AlertPane;
import ua.org.edu.ukma.pncs.gui.utils.Validator;
import ua.org.edu.ukma.pncs.app.client.ClientNetworkFacade;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;

import java.awt.*;
import java.util.List;

public class AddProductDialogController {

	@FXML
	public TextField prodNameField;
	@FXML
	public TextArea prodDescriptionArea;
	@FXML
	public TextField prodManufacField;
	@FXML
	public ComboBox<Group> groupNameDropdown;
	@FXML
	public Spinner<Double> prodPriceSpinner;
	@FXML
	public Spinner<Integer> prodQuantitySpinner;

	@FXML
	public Button createProductButton;
	@FXML
	public Tooltip nameToolTip;

	@FXML
	public Label prodNameMsg;
	@FXML
	public Label prodGroupMsg;
	@FXML
	public Label prodManufacturerMsg;
	@FXML
	public Label prodPriceMsg;
	@FXML
	public Label prodQuantityMsg;
	@FXML
	public Label prodDescriptionMsg;
	@FXML
	public Label productId;
	@FXML
	public Button cancelButton;

	private List<Group> groups;

	@FXML
	public javafx.scene.Group loadingGroup;

	LoadingAnimation loadingAnimation;

	volatile private static Product editProduct;

	private Product productToEdit = null;

	private static boolean isError = false;

	private static String serverError = "error";

	@FXML
	public void initialize() {
		loadingAnimation = new LoadingAnimation(loadingGroup);

		// Value factory.
		SpinnerValueFactory<Integer> quantityFactory = //
			new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0, 1);
		prodQuantitySpinner.setValueFactory(quantityFactory);

		SpinnerValueFactory<Double> priceFactory = //
			new SpinnerValueFactory.DoubleSpinnerValueFactory(0, Double.MAX_VALUE, 10., 1);
		prodPriceSpinner.setValueFactory(priceFactory);

		Validator.setUpTextValidation(prodNameField, prodNameMsg);
		Validator.setUpTextValidation(prodManufacField, prodManufacturerMsg);
		Validator.setUpDoubleValidation(prodPriceSpinner);
		Validator.setUpIntegerValidation(prodQuantitySpinner);

		productId.setText("New Product");

		editProduct = null;
	}

	@FXML
	void btnAddProductClicked(ActionEvent event) {
		if (prodNameField.getText().equals("") || prodManufacField.getText().equals("")) {
			Toolkit.getDefaultToolkit().beep();
			return;
		}

		Thread loadingThread = new Thread(() -> {
			Platform.runLater(() -> {
				loadingStart();
			});

			if (productToEdit == null)
				tryToCreate();
			else
				tryToUpdate();

			Platform.runLater(() -> {
				loadingStop();
				if (editProduct != null) {
					ProductTable.addProduct(editProduct);
					editProduct = null;
					closeStage(event);
				} else if(isError){
					AlertPane.showError("Server error", serverError);
					isError = false;
				}
			});

		});

		loadingThread.start();
	}

	private void tryToCreate() {
		String name = prodNameField.getText().trim();
		Group group = groupNameDropdown.getSelectionModel().getSelectedItem();
		String description = prodDescriptionArea.getText();
		String manufacturer = prodManufacField.getText().trim();
		Double price = Double.parseDouble(prodPriceSpinner.getEditor().getText().replaceAll(",","."));
		Integer quantity = Integer.parseInt(prodQuantitySpinner.getEditor().getText());

		int timeout = 0;

		System.out.println("PRICE " + price);
		System.out.println("QUANTITY " + quantity);

		ClientNetworkFacade.addProduct(new Product(group, name, manufacturer,description,quantity, price));

		while (editProduct == null) {
			try {
				Thread.sleep(100);
				timeout += 100;
				if (timeout > 5000) return;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void tryToUpdate() {
		String name = prodNameField.getText().trim();
		Group group = groupNameDropdown.getSelectionModel().getSelectedItem();
		String description = prodDescriptionArea.getText();
		String manufacturer = prodManufacField.getText().trim();
		Double price = Double.parseDouble(prodPriceSpinner.getEditor().getText().replaceAll(",","."));
		Integer quantity = Integer.parseInt(prodQuantitySpinner.getEditor().getText());

		int timeout = 0;

		ClientNetworkFacade.updateProduct(new Product(productToEdit.getId(),group, name, manufacturer,description,quantity, price));

		while (editProduct == null) {
			try {
				Thread.sleep(100);
				timeout += 100;
				if (timeout > 5000) return;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void setGroupList(List<Group> groupList) {
		this.groups = groupList;

		for (Group gr : groups) {
			groupNameDropdown.getItems().add(gr);
		}

		groupNameDropdown.setValue(groupList.get(0));
	}

	private void closeStage(ActionEvent event) {
		Node source = (Node) event.getSource();
		Stage stage = (Stage) source.getScene().getWindow();
		stage.close();
	}

	public void setupEditProduct(Product product) {
		productId.setText("Id:" + product.getId());
		productToEdit = product;

		prodNameField.setText(product.getName());
		groupNameDropdown.setValue(product.getGroup());
		prodDescriptionArea.setText(product.getDescription());
		prodManufacField.setText(product.getManufacturer());
		prodPriceSpinner.getEditor().setText(Double.toString(product.getPrice()));
		prodQuantitySpinner.getEditor().setText(Integer.toString(product.getQuantity()));

		createProductButton.setText("Save");
	}

	public void btnCancelClicked(ActionEvent actionEvent) {
		closeStage(actionEvent);
	}

	private void loadingStart() {
		prodNameField.setEditable(false);
		prodManufacField.setEditable(false);
		prodDescriptionArea.setEditable(false);
		prodPriceSpinner.setEditable(false);
		prodQuantitySpinner.setEditable(false);
		groupNameDropdown.setDisable(true);
		createProductButton.setVisible(false);
		cancelButton.setVisible(false);
		loadingAnimation.loadingAnimationStart();
	}

	private void loadingStop() {
		loadingAnimation.loadingAnimationStop();
		prodNameField.setEditable(true);
		prodManufacField.setEditable(true);
		prodDescriptionArea.setEditable(true);
		prodPriceSpinner.setEditable(true);
		prodQuantitySpinner.setEditable(true);
		groupNameDropdown.setDisable(false);
		createProductButton.setVisible(true);
		cancelButton.setVisible(true);
	}

	public static void receiveProduct(Product product) {
		editProduct = product;
	}

	public static void serverError(String error){
		isError = true;
		serverError = error;
	}

}
