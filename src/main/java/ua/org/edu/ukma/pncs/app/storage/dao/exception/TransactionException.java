package ua.org.edu.ukma.pncs.app.storage.dao.exception;

public class TransactionException extends Exception {
	public TransactionException(String reason) {
		super(reason);
	}
}
