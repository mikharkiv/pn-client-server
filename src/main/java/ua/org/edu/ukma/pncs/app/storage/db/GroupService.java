package ua.org.edu.ukma.pncs.app.storage.db;

import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Utils;
import ua.org.edu.ukma.pncs.app.storage.dao.GroupDao;
import ua.org.edu.ukma.pncs.app.storage.dao.exception.TransactionException;

import java.sql.Connection;
import java.util.List;

public class GroupService {
	private final GroupDao groupDao;

	public GroupService(Connection connection) {
		this.groupDao = new GroupDao(connection);
	}

	/**
	 * Method for testing purposes
	 *
	 * @return
	 */
	synchronized public void clear() {
		try {
			groupDao.clear();
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
		}
	}

	/**
	 * Method for testing purposes
	 *
	 * @return Group
	 */
	public Group getGroup(String groupName) {
		return groupDao.get(groupName).orElseGet(
			() -> Utils.emptyGroup());
	}

	public List<Group> getAll() {
		return groupDao.getAll();
	}

	public Group getGroup(int id) {
		return groupDao.get(id).orElseGet(
			() -> Utils.emptyGroup());
	}

	synchronized public DBService.Result deleteGroup(int id) {
		try {
			groupDao.delete(id);
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

	synchronized public DBService.Result createGroup(Group group) {
		try {
			groupDao.save(group);
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

	synchronized public DBService.Result updateGroup(Group group) {
		try {
			groupDao.update(group);
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

	@Deprecated
	synchronized public DBService.Result createGroup(String groupName) {
		return createGroup(new Group(groupName));
	}

	@Deprecated
	synchronized public DBService.Result deleteGroup(Group group) {
		try {
			groupDao.delete(group);
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

	@Deprecated
	synchronized public DBService.Result deleteGroup(String groupName) {
		return deleteGroup(new Group(groupName));
	}
}
