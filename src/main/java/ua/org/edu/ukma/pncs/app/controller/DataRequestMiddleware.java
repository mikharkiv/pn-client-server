package ua.org.edu.ukma.pncs.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.primitives.UnsignedLong;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.controller.model.AbstractMiddleware;
import ua.org.edu.ukma.pncs.app.controller.model.MiddlewareMessage;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;
import ua.org.edu.ukma.pncs.app.storage.model.User;

import java.io.IOException;
import java.util.List;

public class DataRequestMiddleware extends AbstractMiddleware {
	private final DBService database;

	public DataRequestMiddleware(DBService database) {
		super();
		this.database = database;
	}

	@Override
	protected void handle(MiddlewareMessage p) {
		if (p.getPacket() != null) {
			ObjectMapper mapper = new ObjectMapper();

			Message input = p.getPacket().getMessage();
			Message answer = new Message();
			answer.setCType(input.getCType());
			answer.setBUserID(input.getBUserID());
			String[] body = input.getArgs();
			String answerMsg = "";

			DBService.Result result = null;
			DBService.Result transactionResult = null;

			Group group;
			Product product;

			try {
				switch (MessageCodes.values()[input.getCType()]) {
					case GROUP_GET_GROUPS:
						answerMsg = mapper.writeValueAsString(database.getGroupService().getAll());
						break;
					case GROUP_GET_PRODUCTS:
						group = database.getGroupService().getGroup(Integer.parseInt(body[0]));
						if (group.getName().equals("")) {
							transactionResult = DBService.Result.FAILED;
							break;
						}
						answerMsg = mapper.writeValueAsString(database.getProductsInGroup(Integer.parseInt(body[0])));
						break;
					case GROUP_GET_GROUP_DETAILS:
						group = database.getGroupService().getGroup(Integer.parseInt(body[0]));
						if (group.getName().equals("")) {
							transactionResult = DBService.Result.FAILED;
							break;
						}
						List<Product> products = database.getProductsInGroup(Integer.parseInt(body[0]));
						int productsCount = products.size();
						double allgemeinPrice = 0;
						for (Product prod : products)
							allgemeinPrice += (double) prod.getQuantity() * prod.getPrice();
						answerMsg = mapper.writeValueAsString(String.valueOf(productsCount).concat(Message.DELIM).concat(String.valueOf(allgemeinPrice)));
						break;
					case GROUP_CREATE:
						result = database.getGroupService().createGroup(mapper.readValue(input.getMessage(), Group.class));
						if (result == DBService.Result.SUCCESS) {
							answerMsg = mapper.writeValueAsString(database.getGroupService().getGroup(body[0]));
						} else {
							transactionResult = result;
						}
						break;
					case GROUP_UPDATE:
						group = mapper.readValue(input.getMessage(), Group.class);
						result = database.getGroupService().updateGroup(group);
						if (result == DBService.Result.SUCCESS) {
							answerMsg = mapper.writeValueAsString(group);
						} else {
							transactionResult = result;
						}
						break;
					case GROUP_REMOVE:
						group = database.getGroupService().getGroup(Integer.parseInt(body[0]));
						result = database.getGroupService().deleteGroup(Integer.parseInt(body[0]));
						if (result == DBService.Result.SUCCESS) {
							answerMsg = "s".concat(String.valueOf(group.getId()));
						} else {
							transactionResult = result;
						}
						break;
					case PRODUCT_GET:
						product = database.getProductService().getProduct(Integer.parseInt(body[0]));
						answerMsg = (product == null ? String.valueOf(DBService.Result.FAILED.ordinal()) :
								mapper.writeValueAsString(product));
						break;
					case PRODUCT_UPDATE:
						product = database.getProductService().getProduct(mapper.readValue(input.getMessage(), Product.class).getId());
						if (!product.validate()) {
							transactionResult = DBService.Result.FAILED;
							break;
						}
						Product temp = mapper.readValue(input.getMessage(), Product.class);
						result = database.getProductService().update(temp);
						if (result == DBService.Result.SUCCESS) {
							answerMsg = mapper.writeValueAsString(temp);
						} else {
							transactionResult = result;
						}
						break;
					case PRODUCT_CREATE:
						product = mapper.readValue(input.getMessage(), Product.class);
						if (!product.validate()) {
							transactionResult = DBService.Result.FAILED;
							break;
						}
						result = database.getProductService().createProduct(product);
						if (result == DBService.Result.SUCCESS) {
							answerMsg = mapper.writeValueAsString(product);
						} else {
							transactionResult = result;
						}
						break;
					case PRODUCT_REMOVE:
						result = database.getProductService().deleteProduct(Integer.parseInt(body[0]));
						if (result == DBService.Result.SUCCESS) {
							answerMsg = "s".concat(body[0]);
						} else {
							transactionResult = result;
						}
						break;
					case USER_UPDATE:
						User user = mapper.readValue(input.getMessage(), User.class);
						result = database.getUserService().updateUser(user);
						if (result == DBService.Result.SUCCESS) {
							User tempUser = new User(user.getId(), user.getLogin(), null);
							answerMsg = mapper.writeValueAsString(tempUser);
						} else {
							transactionResult = result;
						}
						break;

					// OLD COMMANDS BELOW - for some reasons
					case HOW_MANY:
						answerMsg = String.valueOf(database.getProductService().howMany(body[0], body[1]));
						break;
					case SUBTRACT_PRODUCTS:
						transactionResult = database.getProductService().subtractProducts(body[0], body[1], Integer.parseInt(body[2]));
						break;
					case ADD_PRODUCTS:
						transactionResult = database.getProductService().plusProducts(body[0], body[1], Integer.parseInt(body[2]));
						break;
					case ADD_NEW_GROUP:
						transactionResult = database.getGroupService().createGroup(body[0]);
						break;
					case ADD_NEW_PRODUCT_TO_GROUP:
						transactionResult = database.getProductService().createProduct(body[0], body[1]);
						break;
					case SET_PRICE:
						transactionResult = database.getProductService().setPrice(body[0], body[1], Double.parseDouble(body[2]));
						break;
					default:
						pass(p);
						return;
				}
			} catch (JsonProcessingException e) {
				transactionResult = DBService.Result.FAILED;
			} catch (ArrayIndexOutOfBoundsException | NumberFormatException | IOException e){
				System.out.println("Wrong message body format");
				transactionResult = DBService.Result.FAILED;
			}
			if (transactionResult != null)
				answerMsg = String.valueOf(transactionResult.ordinal());
			answer.setMessage(answerMsg);
			Packet packet = new Packet((byte) 1, UnsignedLong.ZERO, answer);
			result(packet);
		} else {
			pass(p);
		}
	}
}
