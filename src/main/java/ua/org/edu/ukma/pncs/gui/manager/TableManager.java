package ua.org.edu.ukma.pncs.gui.manager;

import javafx.scene.control.TableView;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class TableManager<T> {

	@Getter @Setter
	String searchProperty = "";

	@Getter
	List<T> list;

	public TableView<T> table;

	public abstract void delete();

	public void update() {
		updateTable();
		updateView();
	}
	protected abstract void updateTable();
	protected abstract void updateView();

	public TableManager(TableView<T> table)
	{
		this.table = table;
	}

}
