package ua.org.edu.ukma.pncs.gui.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.stage.Stage;

public class AlertPane {

	public static void showInfo(String header, String content){
		Alert a = new Alert(Alert.AlertType.INFORMATION);
		a.setHeaderText(header);
		a.setContentText(content);

		DialogPane dialogPane = a.getDialogPane();
		dialogPane.getStylesheets().add(AlertPane.class.getClassLoader().getResource("css/alert_stylesheet.css").toExternalForm());
		dialogPane.getStyleClass().add("myDialog");

		Stage stage = (Stage) dialogPane.getScene().getWindow();
		IconManager.setIcon(stage);

		a.show();
	}

	public static void showError(String header, String content){
		Alert a = new Alert(Alert.AlertType.ERROR);
		a.setHeaderText(header);
		a.setContentText(content);

		DialogPane dialogPane = a.getDialogPane();
		dialogPane.getStylesheets().add(AlertPane.class.getClassLoader().getResource("css/alert_stylesheet.css").toExternalForm());
		dialogPane.getStyleClass().add("myDialog");

		Stage stage = (Stage) dialogPane.getScene().getWindow();
		IconManager.setIcon(stage);

		a.show();
	}

}
