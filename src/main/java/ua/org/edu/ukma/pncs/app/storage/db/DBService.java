package ua.org.edu.ukma.pncs.app.storage.db;

import javafx.util.Pair;
import lombok.Getter;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;

import ua.org.edu.ukma.pncs.app.storage.model.User;
import ua.org.edu.ukma.pncs.app.storage.model.Utils;
import ua.org.edu.ukma.pncs.app.storage.dao.GroupDao;
import ua.org.edu.ukma.pncs.app.storage.dao.ProductDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class DBService {
	public enum Result {NEGATIVE_QUANTITY, FAILED, SUCCESS, EMPTY, NOT_ENOUGH_PRODUCTS}

	private final Connection connection;
	@Getter
	private final ProductService productService;
	@Getter
	private final GroupService groupService;
	@Getter
	private final UserService userService;

	private DBService(Connection connection) {
		this.connection = connection;
		productService = new ProductService(connection);
		groupService = new GroupService(connection);
		userService = new UserService(connection);
	}

	public static DBService instantiateService(Connection connection) {
		return new DBService(connection);
	}

	/**
	 * Instantiate database service with already set up database for test usage
	 * ONLY CALL THIS WHEN DBHelper.isTest == true
	 *
	 * @param connection
	 * @return
	 */
	public static DBService instantiateTestService(Connection connection) {
		DBService resultService = new DBService(connection);
		if (DBHelper.isTest) {
			try {
				if (DBHelper.dropTables) {
					connection.createStatement().executeUpdate("DROP TABLE IF EXISTS " + ProductDao.TABLE_NAME);
					connection.createStatement().executeUpdate("DROP TABLE IF EXISTS " + GroupDao.TABLE_NAME);
				}
				PreparedStatement groupsTable = connection.prepareStatement(GroupDao.CREATE_TABLE);
				int result = groupsTable.executeUpdate();
				PreparedStatement productsTable = connection.prepareStatement(ProductDao.CREATE_TABLE);
				result = productsTable.executeUpdate();

				Group def = new Group("default");
				Group fruit = new Group("Fruit");
				Group vegetables = new Group("Vegetables");
				Group furniture = new Group("Furniture");

				Product apples = new Product(fruit, "Apples", 1200, 10.5);
				Product tomatoes = new Product(vegetables, "Tomatoes", 5500, 7.30);
				Product tables = new Product(furniture, "Tables", 670, 120.);

				User user = new User("login", "pass");

				resultService.getGroupService().createGroup(def);
				resultService.getGroupService().createGroup(fruit);
				resultService.getGroupService().createGroup(vegetables);
				resultService.getGroupService().createGroup(furniture);

				resultService.getProductService().createProduct(apples);
				resultService.getProductService().createProduct(tomatoes);
				resultService.getProductService().createProduct(tables);

				resultService.getUserService().createUser(user);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultService;
	}

	/**
	 * Method for testing purposes
	 *
	 * @return
	 */
	public Map<Group, List<Product>> getProductsToGroups() {
		List<Group> groups = getGroupService().getAll();
		List<Product> prods = getProductService().getAll();
		Map<Group, List<Product>> resultMap = new HashMap<>();
		groups.forEach(group -> resultMap.put(group, prods.stream().filter(product -> product.getGroup().equals(group)).collect(Collectors.toList())));
		return resultMap;
	}

	/**
	 * Method for testing purposes
	 *
	 * @return
	 */
	public Pair<Group, List<Product>> getProductsToGroup(String groupName) {
		Group group = getGroupService().getGroup(groupName);
		if (group.equals(Utils.emptyGroup())) return new Pair<>(null, null);
		List<Product> prods = getProductService().getAll();
		return new Pair(group, prods.stream().filter(product -> product.getGroup().equals(group)).collect(Collectors.toList()));
	}

	public List<Product> getProductsInGroup(int groupName) {
		Group group = getGroupService().getGroup(groupName);
		if (group.equals(Utils.emptyGroup())) return new ArrayList<>();
		List<Product> prods = getProductService().getAll();
		return prods.stream().filter(product -> product.getGroup().equals(group)).collect(Collectors.toList());
	}

	/**
	 * Method for testing purposes
	 *
	 * @return
	 */
	synchronized public void clear() {
		getProductService().clear();
		getGroupService().clear();
	}
}
