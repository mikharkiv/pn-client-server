package ua.org.edu.ukma.pncs.app.util;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class DESEncryptionManager {

	public static final String ALGORITHM = "DES";

	public static byte[] encryptData(byte[] key, byte[] data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		final SecretKeySpec keySpec = new SecretKeySpec(key, "DES");
		final Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);
		return cipher.doFinal(data);
	}

	public static byte[] decryptData(byte[] key, byte[] encryptedData) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		final SecretKeySpec keySpec = new SecretKeySpec(key, "DES");
		final Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, keySpec);
		return cipher.doFinal(encryptedData);
	}

}
