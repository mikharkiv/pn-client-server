package ua.org.edu.ukma.pncs.gui.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ua.org.edu.ukma.pncs.gui.manager.GroupTable;
import ua.org.edu.ukma.pncs.gui.manager.ProductTable;
import ua.org.edu.ukma.pncs.gui.utils.AlertPane;
import ua.org.edu.ukma.pncs.gui.utils.IconManager;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;
import ua.org.edu.ukma.pncs.app.storage.model.User;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class StorageController {
	@FXML
	public TableView<Product> productTable;
	@FXML
	public TableView<Group> groupTable;
	@FXML
	public HBox addProductBox;
	@FXML
	public HBox addGroupBox;
	@FXML
	public MenuItem closeItem;
	@FXML
	public MenuItem addProductItem;
	@FXML
	public MenuItem addGroupItem;
	@FXML
	public MenuItem refreshItem;
	@FXML
	public MenuItem logOutItem;
	@FXML
	public MenuItem aboutItem;
	@FXML
	public TextField searchField;
	@FXML
	public MenuBar menuBar;
	@FXML
	public MenuItem statisticsItem;
	@FXML
	public MenuItem userEditItem;
	@FXML
	public ChoiceBox groupChoiceBox;
	@FXML
	public ComboBox groupComboBox;
	@FXML
	public TabPane tabPane;

	private GroupTable groupTableManager;
	private ProductTable productTableManager;

	private static String ALL_GROUPS = "All Groups";

	public static User currentUser;

	@FXML
	public void initialize() {
		groupTableManager = new GroupTable(groupTable);
		productTableManager = new ProductTable(productTable);
		setHotKeys();

		groupComboBox.getItems().add(ALL_GROUPS);
		groupComboBox.setValue(ALL_GROUPS);
		groupComboBox.getItems().addAll(groupTableManager.getList());
		groupComboBox.setVisibleRowCount(4);
	}

	private void update() {
		groupTableManager.update();
		productTableManager.update();
		groupComboBox.getItems().retainAll(ALL_GROUPS);
		groupComboBox.getItems().addAll(groupTableManager.getList());
		groupComboBox.setValue(ALL_GROUPS);
	}

	private void setHotKeys() {
		closeItem.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN));
		refreshItem.setAccelerator(new KeyCodeCombination(KeyCode.R));
		logOutItem.setAccelerator(new KeyCodeCombination(KeyCode.E));
		userEditItem.setAccelerator(new KeyCodeCombination(KeyCode.U));

		addProductItem.setAccelerator(new KeyCodeCombination(KeyCode.DIGIT1));
		addGroupItem.setAccelerator(new KeyCodeCombination(KeyCode.DIGIT2));

		aboutItem.setAccelerator(new KeyCodeCombination(KeyCode.A));
		statisticsItem.setAccelerator(new KeyCodeCombination(KeyCode.S));
	}

	public void chooseProductTab(Event event) {
		addProductBox.setVisible(true);
		addGroupBox.setVisible(false);
	}

	public void chooseGroupTab(Event event) {
		addProductBox.setVisible(false);
		addGroupBox.setVisible(true);
	}

	public void productContextMenu(ContextMenuEvent contextMenuEvent) {
		ContextMenu contextMenu = new ContextMenu();

		MenuItem item1 = new MenuItem("Add");
		item1.setOnAction(event -> onAddProductAction(event));

		MenuItem item2 = new MenuItem("Delete");
		item2.setOnAction(event -> onDelProductAction(event));

		MenuItem item3 = new MenuItem("Edit");
		item3.setOnAction(event -> onEditProductAction(event));

		MenuItem item4 = new MenuItem("Plus/Subtract");
		item4.setOnAction(event -> onPlusSubtractProductAction(event));

		MenuItem item5 = new MenuItem("Info");
		item5.setOnAction(event -> onProductInfoAction(event));

		MenuItem item6 = new MenuItem("Statistics");
		item6.setOnAction(event -> onStatisticsAction(event));

		Product product = productTable.getSelectionModel().getSelectedItem();
		if (product == null){
			contextMenu.getItems().addAll(item1, item6);
		}
		else
		{
			contextMenu.getItems().addAll(item1, item2, item3, item4, item5);
		}
		contextMenu.show((Stage) ((Node) contextMenuEvent.getSource()).getScene().getWindow(), contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY());
	}

	public void groupContextMenu(ContextMenuEvent contextMenuEvent) {
		ContextMenu contextMenu = new ContextMenu();

		MenuItem item1 = new MenuItem("Add");
		item1.setOnAction(event -> onAddGroupAction(event));

		MenuItem item2 = new MenuItem("Delete");
		item2.setOnAction(event -> onDelGroupAction(event));

		MenuItem item3 = new MenuItem("Edit");
		item3.setOnAction(event -> onEditGroupAction(event));

		MenuItem item4 = new MenuItem("Info");
		item4.setOnAction(event -> onGroupInfoAction(event));

		MenuItem item5 = new MenuItem("Go To Group");
		item5.setOnAction(event -> onGoToGroupAction(event));

		MenuItem item6 = new MenuItem("Stats");
		item6.setOnAction(event -> onStatsAction(event));

		MenuItem item7 = new MenuItem("Statistics");
		item7.setOnAction(event -> onStatisticsAction(event));

		Group group = groupTable.getSelectionModel().getSelectedItem();
		if (group == null){
			contextMenu.getItems().addAll(item1, item5, item7);
		}else{
			contextMenu.getItems().addAll(item1, item2, item3, item4, item5, item6);
		}
		contextMenu.show(((Node) contextMenuEvent.getSource()).getScene().getWindow(), contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY());
	}


	private void showDialog(Parent parent) {
		Scene scene = new Scene(parent);
		Stage stage = new Stage();
		scene.getStylesheets().add(getClass().getClassLoader().getResource("css/text-field-red-border.css").toExternalForm());
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setScene(scene);
		stage.setResizable(false);

		IconManager.setIcon(stage);

		stage.showAndWait();

		update();
	}

	//A C T I O N S


	@FXML
	void onPlusSubtractProductAction(ActionEvent event) {
		Product product = productTable.getSelectionModel().getSelectedItem();
		if (product == null) return;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/AddSubtractProductsDialog.fxml"));
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		PlustSubtractDialogController dialogController = fxmlLoader.<PlustSubtractDialogController>getController();

		dialogController.loadProduct(product);

		showDialog(parent);
	}

	void onGoToGroupAction(ActionEvent event) {
		Group group = groupTable.getSelectionModel().getSelectedItem();
		if (group == null)groupComboBox.setValue(ALL_GROUPS);
		else groupComboBox.setValue(group);
		onGroupSelected(event);
		tabPane.getSelectionModel().selectFirst();
		chooseProductTab(event);
	}

	void onStatsAction(ActionEvent event){
		Group group = groupTable.getSelectionModel().getSelectedItem();
		if (group == null) return;

		List<Product> prods = productTableManager.getList().stream().filter(product -> product.getGroup().equals(group)).collect(Collectors.toList());
		double cost = 0;

		for(Product p:prods){
			cost += p.getQuantity() * p.getPrice();
		}

		StringBuilder sb = new StringBuilder();
		sb.append("Your Group " + group.toString() + " contains " + prods.size() + " products at cost " + cost + "$");
		AlertPane.showInfo("Statistics", sb.toString());
	}

	@FXML
	void onAddProductAction(ActionEvent event) {
		if (groupTableManager.getList().size() < 1) {
			onAddGroupAction(event);
		} else {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/CreateProductDialog.fxml"));
			Parent parent = null;
			try {
				parent = fxmlLoader.load();
			} catch (IOException e) {
				e.printStackTrace();
			}
			AddProductDialogController dialogController = fxmlLoader.<AddProductDialogController>getController();

			dialogController.setGroupList(groupTableManager.getList());

			showDialog(parent);
		}
	}

	@FXML
	void onAddGroupAction(ActionEvent event) {
		Parent parent = null;
		try {
			parent = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/CreateGroupDialog.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		showDialog(parent);
	}

	public void onEditProductAction(ActionEvent actionEvent) {
		Product product = productTable.getSelectionModel().getSelectedItem();
		if (product == null) return;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/CreateProductDialog.fxml"));
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		AddProductDialogController dialogController = fxmlLoader.<AddProductDialogController>getController();

		dialogController.setGroupList(groupTableManager.getList());
		dialogController.setupEditProduct(product);

		showDialog(parent);
	}

	public void onEditGroupAction(ActionEvent actionEvent){
		Group group = groupTable.getSelectionModel().getSelectedItem();
		if (group == null) return;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/CreateGroupDialog.fxml"));
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		AddGroupDialogController dialogController = fxmlLoader.<AddGroupDialogController>getController();
		dialogController.setupEditGroup(group);

		showDialog(parent);
	}

	public void onAboutAction(ActionEvent actionEvent) {
		AlertPane.showInfo("About", "This app was developed by Michael Postnikov and Vadym Nakytniak");
	}

	public void onStatisticsAction(ActionEvent actionEvent) {
		List<Product> prods = productTableManager.getList();
		double cost = 0;

		for(Product p:prods){
			cost += p.getQuantity() * p.getPrice();
		}

		StringBuilder sb = new StringBuilder();
		sb.append("Your Storage contains ");
		sb.append(productTableManager.getList().size() + " products at cost " + cost + "$");
		AlertPane.showInfo("Statistics", sb.toString());
	}

	public void onCloseAction(ActionEvent actionEvent) {
		Platform.exit();
		System.exit(0);
	}

	public void onRefreshAction(ActionEvent actionEvent) {
		productTableManager.update();
		groupTableManager.update();
	}

	public void onLogOutAction(ActionEvent actionEvent) {
		Stage window = (Stage) menuBar.getScene().getWindow();

		Parent parent = null;
		try {
			parent = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/Authorization.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		currentUser = null;

		Scene scene = new Scene(parent, 244, 351);
		window.setHeight(371);
		window.setWidth(244);
		window.setResizable(false);
		window.setScene(scene);
		window.show();
	}

	public void onProductInfoAction(ActionEvent actionEvent) {
		Product product = productTable.getSelectionModel().getSelectedItem();
		if (product == null) return;
		AlertPane.showInfo(product.getName(), product.info());
	}

	public void onGroupInfoAction(ActionEvent actionEvent) {
		Group group = groupTable.getSelectionModel().getSelectedItem();
		if (group == null) return;
		AlertPane.showInfo(group.getName(), group.info());
	}

	public void onSearchAction(ActionEvent actionEvent) {
		productTableManager.setSearchProperty(searchField.getText().trim());
		groupTableManager.setSearchProperty(searchField.getText().trim());
		update();
	}

	public void onDelGroupAction(ActionEvent actionEvent) {
		groupTableManager.delete();
		update();
	}

	public void onDelProductAction(ActionEvent actionEvent) {
		productTableManager.delete();
		update();
	}

	public void onEditUserAction(ActionEvent actionEvent) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/EditUserDialog.fxml"));
		Parent parent = null;
		try {
			parent = fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		EditUserController dialogController = fxmlLoader.<EditUserController>getController();

		dialogController.setupUser();

		showDialog(parent);
	}

	public void onGroupSelected(ActionEvent actionEvent) {
		Object value = groupComboBox.getValue();
		if(value == null) value = ALL_GROUPS;
		if (value.toString().equals(ALL_GROUPS))
			productTableManager.setGroupProperty(null);
		else
			productTableManager.setGroupProperty((Group) value);
		productTableManager.update();
	}

	public static void receiveUser(User user) {
		currentUser = user;
	}
}
