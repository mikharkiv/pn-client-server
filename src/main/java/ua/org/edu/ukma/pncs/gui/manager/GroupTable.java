package ua.org.edu.ukma.pncs.gui.manager;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import ua.org.edu.ukma.pncs.app.client.ClientNetworkFacade;
import ua.org.edu.ukma.pncs.app.storage.model.Group;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class GroupTable extends TableManager<Group> {

	public GroupTable(TableView<Group> table) {
		super(table);
		update();
	}

	public void delete() {
		ClientNetworkFacade.removeGroup(table.getSelectionModel().getSelectedItem().getId());
	}

	protected void updateView() {
		table.getItems().remove(0, table.getItems().size());
		table.getItems().addAll(list);
		if (!searchProperty.isEmpty()) {
			List<Group> newList = list.stream().filter(group -> group.getName().toLowerCase().contains(searchProperty.toLowerCase())).collect(Collectors.toList());
			table.getItems().retainAll(newList);
		}
	}

	protected void updateTable() {
		ClientNetworkFacade.getGroups();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		list = groups;

		ProductTable.noProducts();
		for(Group gr: groups){
			ClientNetworkFacade.getGroupProducts(gr.getId());
		}
	}

	public static void initTable(TableView productTable) {
		TableColumn idColumn = new TableColumn("ID");
		idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

		TableColumn nameColumn = new TableColumn("Name");
		nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

		productTable.getColumns().addAll(idColumn, nameColumn);
	}

	public static List<Group> groups;

	public static void setGroups(Group[] groups){
		GroupTable.groups = new ArrayList();
		Collections.addAll(GroupTable.groups , groups);
	}

	public static void addGroup(Group group){
		if (GroupTable.groups == null)
			GroupTable.groups = new ArrayList();
		GroupTable.groups.add(group);
		System.out.println(ProductTable.products);
	}

}
