package ua.org.edu.ukma.pncs.app.network.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.nio.ByteBuffer;

@EqualsAndHashCode(callSuper = true)
@Data
public class KeyExchangeMessage extends Message {

	public KeyExchangeMessage(Integer cType, Integer bUserId, byte[] key) {
		super(cType, bUserId, "Key");
		this.key = key;
	}

	private byte[] key;

	public int getSize() {
		return HEADER_SIZE + key.length;
	}

	public int getMessageSize() {
		return key.length;
	}

	public byte[] toByteArray() {
		return ByteBuffer.allocate(getSize())
			.putInt(getCType())
			.putInt(getBUserID())
			.put(key)
			.array();
	}

}
