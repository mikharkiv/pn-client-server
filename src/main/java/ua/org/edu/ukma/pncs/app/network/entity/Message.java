package ua.org.edu.ukma.pncs.app.network.entity;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.nio.ByteBuffer;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
	public static final String DELIM = "$";
	private static final String WRAPPED_DELIM = "\\$";
	/**
	 * Size of header (in bytes)
	 * Header is cType and bUserID
	 */
	public static final int HEADER_SIZE = Integer.BYTES * 2;
	/**
	 * Charset of the message
	 */
	public static final String CHARSET = "utf8";

	public static final int MAX_MESSAGE_SIZE = 500;

	private Integer cType;
	private Integer bUserID;
	private String message;

	/**
	 * @return size of message with header (bytes)
	 */
	public int getSize() {
		return HEADER_SIZE + message.length();
	}

	/**
	 * @return size of message without header (bytes)
	 */
	public int getMessageSize() {
		return message.length();
	}

	/**
	 * @return message with header in byte array
	 */
	public byte[] toByteArray() {
		byte[] msgBytes = message.getBytes();
		return ByteBuffer.allocate(getSize())
			.putInt(cType)
			.putInt(bUserID)
			.put(msgBytes)
			.array();
	}

	public String[] getArgs() {
		return message.split(WRAPPED_DELIM);
	}
}
