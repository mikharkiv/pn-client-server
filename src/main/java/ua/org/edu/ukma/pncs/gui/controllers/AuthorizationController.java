package ua.org.edu.ukma.pncs.gui.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import ua.org.edu.ukma.pncs.gui.animations.LoadingAnimation;
import ua.org.edu.ukma.pncs.gui.manager.GroupTable;
import ua.org.edu.ukma.pncs.gui.manager.ProductTable;
import ua.org.edu.ukma.pncs.gui.utils.AlertPane;
import ua.org.edu.ukma.pncs.gui.utils.Validator;
import ua.org.edu.ukma.pncs.app.client.ClientNetworkFacade;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;
import ua.org.edu.ukma.pncs.app.network.model.AbstractServer;
import ua.org.edu.ukma.pncs.app.network.tcp.TCPUser;

import java.awt.*;
import java.io.IOException;

public class AuthorizationController {
	@FXML
	public TextField loginField;
	@FXML
	public PasswordField passwordField;
	@FXML
	public Button loginButton;
	@FXML
	public Label storageLabel;
	@FXML
	public javafx.scene.Group loadingGroup;
	@FXML
	public Button registerButton;
	@FXML
	public Label loginMsg;
	@FXML
	public Label passwordMsg;

	LoadingAnimation loadingAnimation;

	volatile private static boolean connectionEstablished = false;
	volatile private static boolean loggedIn = false;
	volatile private static boolean wrongUser = false;

	private TCPUser user = null;

	@FXML
	public void initialize() {
		loadingAnimation = new LoadingAnimation(loadingGroup);
		Validator.setUpTextValidation(loginField, loginMsg);
		Validator.setUpPasswordValidation(passwordField, passwordMsg);
	}

	public void loginButtonClicked(MouseEvent mouseEvent) throws IOException {
		if (loginField.getText().equals("") || passwordField.getText().length() < 4) {
			Toolkit.getDefaultToolkit().beep();
			return;
		}

		new Thread(() -> {
			Platform.runLater(() -> {
				loadingStart();
			});

			final Scene scene = tryToLogin();

			Platform.runLater(() -> {
				loadingStop();
				if (scene != null) {
					Stage window = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
					window.setResizable(false);
					window.setScene(scene);

					window.setWidth(760);
					window.setHeight(580);

					window.setOnCloseRequest(event -> {
						Platform.exit();
						System.exit(0);
					});

					loggedIn = false;
				} else {
					if (wrongUser) {
						AlertPane.showError("Wrong user data", "Invalid login or password  :/");
						wrongUser = false;
					} else if (user == null) {
						AlertPane.showError("Server is down", "Server is not working currently, try later  :/");
					} else
						AlertPane.showError("Access not permitted", "Something went wrong :/");
				}
			});

		}).start();
	}

	private Scene tryToLogin() {
		if (!connectionEstablished)
			user = ClientNetworkFacade.connect("127.0.0.1", AbstractServer.PORT);

		int timeout = 0;

		while (!connectionEstablished) {
			try {
				Thread.sleep(100);
				timeout += 100;
				if (timeout > 5000) return null;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (!connectionEstablished && user == null) return null;

		if (connectionEstablished) {
			ClientNetworkFacade.login(loginField.getText(), passwordField.getText());

			timeout = 0;

			while (!loggedIn) {
				try {
					Thread.sleep(100);
					if (wrongUser == true) {
						return null;
					}
					timeout += 100;
					if (timeout > 5000) return null;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			if (loggedIn) {
				Parent parent = null;
				try {
					parent = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/StorageApp.fxml"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				Scene scene = new Scene(parent);

				TableView productTable = (TableView) scene.lookup("#productTable");
				TableView groupTable = (TableView) scene.lookup("#groupTable");

				ProductTable.initTable(productTable);
				GroupTable.initTable(groupTable);

				return scene;
			} else return null;
		} else
			return null;
	}


	public void regButtonClicked(MouseEvent mouseEvent) {
		/*
		new Thread(() -> {
			Platform.runLater(() -> {
				loadingStart();
			});

			final Scene scene = tryToLogin();

			Platform.runLater(() -> {
				if (scene != null) {
					Stage window = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
					window.setResizable(false);
					window.setScene(scene);
					window.setWidth(760);
					window.setHeight(580);
					window.show();
				} else {
					loadingStop();
					AlertPane.showError("Access not permitted", "Invalid password");
				}
			});

		}).start();
		*/
	}

	private void loadingStart() {
		loginButton.setVisible(false);
		loadingAnimation.loadingAnimationStart();
		loginField.setEditable(false);
		passwordField.setEditable(false);
		//registerButton.setVisible(false);

	}

	private void loadingStop() {
		loadingAnimation.loadingAnimationStop();
		loginButton.setVisible(true);
		loginField.setEditable(true);
		passwordField.setEditable(true);
		//registerButton.setVisible(true);
	}

	public static void serverResponse(MessageCodes code) {

		System.out.println(code);

		if (code.equals(MessageCodes.CONNECTION_ESTABLISHED)) {
			connectionEstablished = true;
		} else if (code.equals(MessageCodes.LOGIN_SUCCESS)) {
			loggedIn = true;
			wrongUser = false;
		} else if (code.equals(MessageCodes.LOGIN_FAIL)) {
			wrongUser = true;
		}
	}

}
