package ua.org.edu.ukma.pncs.app.exception;

public class WrongMessageCommandException extends MessageException {
	public WrongMessageCommandException(String errorMessage) {
		super(errorMessage);
	}
}
