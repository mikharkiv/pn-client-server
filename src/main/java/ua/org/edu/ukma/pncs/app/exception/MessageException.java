package ua.org.edu.ukma.pncs.app.exception;

public class MessageException extends RuntimeException {
	public MessageException(String errorMessage) {
		super(errorMessage);
	}
}
