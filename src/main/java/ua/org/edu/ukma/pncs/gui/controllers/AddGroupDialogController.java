package ua.org.edu.ukma.pncs.gui.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ua.org.edu.ukma.pncs.gui.animations.LoadingAnimation;
import ua.org.edu.ukma.pncs.gui.manager.GroupTable;
import ua.org.edu.ukma.pncs.gui.utils.AlertPane;
import ua.org.edu.ukma.pncs.gui.utils.Validator;
import ua.org.edu.ukma.pncs.app.client.ClientNetworkFacade;
import ua.org.edu.ukma.pncs.app.storage.model.Group;

import java.awt.*;

public class AddGroupDialogController {

	@FXML
	public TextField groupNameField;
	@FXML
	public TextArea groupDescriptionArea;
	@FXML
	public Button createGroupButton;
	@FXML
	public Label groupNameMsg;

	@FXML
	public javafx.scene.Group loadingGroup;
	@FXML
	public Label groupId;
	@FXML
	public Button cancelButton;

	LoadingAnimation loadingAnimation;

	volatile private static Group editGroup;

	private Group groupToEdit = null;

	private static boolean isError = false;

	private static String serverError = "error";

	@FXML
	public void initialize() {
		loadingAnimation = new LoadingAnimation(loadingGroup);
		Validator.setUpTextValidation(groupNameField, groupNameMsg);

		groupId.setText("New Group");
	}

	@FXML
	void btnAddGroupClicked(ActionEvent event) {
		if (groupNameField.getText().equals("")) {
			Toolkit.getDefaultToolkit().beep();
			return;
		}

		Thread loadingThread = new Thread(() -> {
			Platform.runLater(() -> {
				loadingStart();
			});

			if (groupToEdit != null)
				tryToEdit();
			else
				tryToCreate();

			Platform.runLater(() -> {
				loadingStop();
				if (editGroup != null) {
					GroupTable.addGroup(editGroup);
					editGroup = null;
					closeStage(event);
				} else if(isError){
					AlertPane.showError("Server error", serverError);
					isError = false;
				}
			});

		});

		loadingThread.start();
	}

	private void tryToCreate() {
		String name = groupNameField.getText().trim();
		String description = groupDescriptionArea.getText();

		int timeout = 0;

		ClientNetworkFacade.addGroup(new Group(name, description));

		while (editGroup == null) {
			try {
				Thread.sleep(100);
				timeout += 100;
				if (timeout > 5000) return;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void tryToEdit() {
		int id = groupToEdit.getId();
		String name = groupNameField.getText().trim();
		String description = groupDescriptionArea.getText();

		int timeout = 0;

		ClientNetworkFacade.updateGroup(new Group(id, name, description));

		while (editGroup == null) {
			System.out.println(editGroup);
			try {
				Thread.sleep(100);
				timeout += 100;
				if (timeout > 5000) return;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void closeStage(ActionEvent event) {
		Node source = (Node) event.getSource();
		Stage stage = (Stage) source.getScene().getWindow();
		stage.close();
	}

	public void setupEditGroup(Group group) {
		groupToEdit = group;

		groupId.setText("Id:" + group.getId());
		groupNameField.setText(group.getName());
		groupDescriptionArea.setText(group.getDescription());

		createGroupButton.setText("Save");
	}

	public void btnCancelClicked(ActionEvent actionEvent) {
		closeStage(actionEvent);
	}

	private void loadingStart() {
		createGroupButton.setVisible(false);
		loadingAnimation.loadingAnimationStart();
		groupNameField.setEditable(false);
		groupDescriptionArea.setEditable(false);
		cancelButton.setVisible(false);
	}


	private void loadingStop() {
		createGroupButton.setVisible(true);
		loadingAnimation.loadingAnimationStop();
		groupNameField.setEditable(true);
		groupDescriptionArea.setEditable(true);
		cancelButton.setVisible(true);
	}

	public static void receiveGroup(Group group) {
		editGroup = group;
	}

	public static void serverError(String error){
		isError = true;
		serverError = error;
	}
}
