package ua.org.edu.ukma.pncs.app.controller;

import com.google.common.primitives.UnsignedLong;
import ua.org.edu.ukma.pncs.app.network.entity.KeyExchangeMessage;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.controller.model.AbstractMiddleware;
import ua.org.edu.ukma.pncs.app.controller.model.MiddlewareMessage;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.model.AbstractUser;
import ua.org.edu.ukma.pncs.app.util.ArrayUtils;

import javax.crypto.KeyAgreement;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class KeyExchangeMiddleware extends AbstractMiddleware {
	public static final String KEY_ALGORITHM = "DH";
	private static final int KEY_SIZE = 1024;
	public enum State {CREATED, CONNECTION_ESTABLISHED}

	private State state;
	private PrivateKey privateKey;
	private PublicKey publicKey;
	byte[] clientPubKeyEnc = null;

	public KeyExchangeMiddleware() {
		state = State.CREATED;
		try {
			generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			error(e);
		}
	}

	@Override
	public void handle(MiddlewareMessage p) {
		if (p.getEvent() != null) {
			Event event = p.getEvent();
			if (event.getEventType() == Events.CLIE_LISTENING_STARTED) {
				result(new Packet((byte) 1, UnsignedLong.ONE, new KeyExchangeMessage(MessageCodes.KEY_EXCHANGE.ordinal(), 1, publicKey.getEncoded())));
			} else {
				if (state == State.CONNECTION_ESTABLISHED)
					pass(p);
				// else - YOU SHALL NOT PASS!!!!!!!!!!!!!!!!!!!!!!!!!!!
			}
		} else {
			Message m = p.getPacket().getMessage();
			if (MessageCodes.values()[m.getCType()] == MessageCodes.KEY_EXCHANGE) {
				clientPubKeyEnc = ((KeyExchangeMessage) m).getKey();
				try {
					establishConnection(p.getUser());
				} catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException e) {
					error(e);
				}
				state = State.CONNECTION_ESTABLISHED;
				result(new Packet((byte) 1, UnsignedLong.valueOf(2), new Message(MessageCodes.CONNECTION_ESTABLISHED.ordinal(), 1, "Success")));
			} else {
				if (state == State.CONNECTION_ESTABLISHED)
					pass(p);
				// else - YOU SHALL NOT PASS!!!!!!!!!!!!!!!!!!!!!!!!!!!
			}
		}
	}

	public void generateKeyPair() throws NoSuchAlgorithmException {
		final KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DH");
		keyPairGenerator.initialize(KEY_SIZE);
		final KeyPair keyPair = keyPairGenerator.generateKeyPair();

		privateKey = keyPair.getPrivate();
		publicKey = keyPair.getPublic();
	}

	public void establishConnection(AbstractUser user) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException {
		final KeyFactory serverKeyFac = KeyFactory.getInstance(KEY_ALGORITHM);
		final X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(clientPubKeyEnc);
		final PublicKey clientPubKey = serverKeyFac.generatePublic(x509KeySpec);

		final KeyAgreement keyAgreement = KeyAgreement.getInstance(KEY_ALGORITHM);
		keyAgreement.init(privateKey);

		keyAgreement.doPhase(clientPubKey, true);
		byte[] serverSharedSecret = ArrayUtils.shortenSecretKey(keyAgreement.generateSecret());
		user.setKey(serverSharedSecret);
	}
}
