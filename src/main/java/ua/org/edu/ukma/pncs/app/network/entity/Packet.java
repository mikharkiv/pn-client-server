package ua.org.edu.ukma.pncs.app.network.entity;

import com.github.snksoft.crc.CRC;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import com.google.common.primitives.UnsignedLong;
import ua.org.edu.ukma.pncs.app.exception.PacketValidatingException;
import ua.org.edu.ukma.pncs.app.util.DESEncryptionManager;


@ToString
@EqualsAndHashCode
public class Packet {
	/**
	 * Packet size in bytes without message part
	 */
	public static final int PACKET_SIZE_NO_MESSAGE = Byte.BYTES + Byte.BYTES + Long.BYTES + Integer.BYTES + 2 * Short.BYTES;
	/**
	 * Size of packet's header without CRC checksum
	 */
	public static final int HEADER_SIZE_NO_CRC = Byte.BYTES + Byte.BYTES + Long.BYTES + Integer.BYTES;

	public final static Integer MAX_PACKET_SIZE = HEADER_SIZE_NO_CRC + Short.BYTES + Message.MAX_MESSAGE_SIZE;
	/**
	 * Byte indicating the beginning of the packet(13h)
	 */
	public static final Byte bMagic = 0x13;

	@Getter
	private final Byte bSrc;
	@Getter
	@Setter
	private UnsignedLong bPktId;
	@Getter
	private final Integer wLen;
	@Getter
	private Short wCrc16_1;
	@Getter
	private final Message message;
	@Getter
	private Short wCrc16_2;

	/**
	 * Base constructor of the Packet
	 * Automatically calculates crc16 sums
	 *
	 * @param bSrc
	 * @param bPktId
	 * @param message
	 */
	public Packet(Byte bSrc, UnsignedLong bPktId, Message message) {
		this.bSrc = bSrc;
		this.bPktId = bPktId;
		this.message = message;
		this.wLen = PACKET_SIZE_NO_MESSAGE + message.getSize();

		//wCrc16 sum is calculated right before the packet converting to byte array
		this.wCrc16_1 = 0;
		this.wCrc16_2 = 0;
	}

	/**
	 * Constructs the Packet from an encoded byte array
	 *
	 * @param key
	 * @param encodedPacket
	 * @throws GeneralSecurityException
	 */
	public Packet(byte[] key, byte[] encodedPacket) throws GeneralSecurityException, PacketValidatingException {
		ByteBuffer buffer = ByteBuffer.wrap(encodedPacket);

		Byte expectedBMagic = buffer.get();
		if (!expectedBMagic.equals(Packet.bMagic))
			throw new PacketValidatingException("Unexpected bMagic");

		bSrc = buffer.get();
		bPktId = UnsignedLong.fromLongBits(buffer.getLong());

		Integer packetLength = buffer.getInt();
		Integer encodedMessageLength = packetLength - PACKET_SIZE_NO_MESSAGE;

		wCrc16_1 = buffer.getShort();
		byte[] encodedMessageBytes = new byte[encodedMessageLength];
		buffer.get(encodedMessageBytes);
		wCrc16_2 = buffer.getShort();

		byte[] decodedMessageBytes = encodedMessageBytes;
		if (key != null)
			try {
				decodedMessageBytes = DESEncryptionManager.decryptData(key, encodedMessageBytes);
			} catch (GeneralSecurityException gse) {
				throw new GeneralSecurityException("Error while decrypting the message. Key "+new String(key));
			}

		ByteBuffer decodedMessageBuffer = ByteBuffer.wrap(decodedMessageBytes);

		Short headerChecksum = Packet.calculatedCrc16(Arrays.copyOfRange(encodedPacket, 0, Packet.HEADER_SIZE_NO_CRC));
		if (!wCrc16_1.equals(headerChecksum))
			throw new PacketValidatingException("Wrong header checksum: packet's header is corrupted");
		Short messageChecksum = Packet.calculatedCrc16(encodedMessageBytes);
		if (!wCrc16_2.equals(messageChecksum))
			throw new PacketValidatingException("Wrong message checksum: packet's message is corrupted");

		Integer cType = decodedMessageBuffer.getInt();
		Integer bUserId = decodedMessageBuffer.getInt();

		Integer decodedMessageBodyLength = decodedMessageBuffer.remaining();
		byte[] decodedMessageBody = new byte[decodedMessageBodyLength];
		decodedMessageBuffer.get(decodedMessageBody);

		if (key == null) {
			message = new KeyExchangeMessage(cType, bUserId, decodedMessageBody);
		} else {
			String decodedMsg = new String(decodedMessageBody);
			message = new Message(cType, bUserId, decodedMsg);
		}

		wLen = PACKET_SIZE_NO_MESSAGE + message.getSize();
	}

	/**
	 * Method for calculating CRC16 of the I and II parts of the packet
	 *
	 * @param packetPart
	 * @return crc16 in short
	 */
	public static Short calculatedCrc16(byte[] packetPart) {
		return (short) CRC.calculateCRC(CRC.Parameters.CRC16, packetPart);
	}

	/**
	 * @param key
	 * @return encoded message in byte[], null if some exception does occur
	 */
	public byte[] toByteArray(byte[] key) throws GeneralSecurityException {
		byte[] messagePartBytes = message.toByteArray();

		byte[] encryptedMessagePartBytes = messagePartBytes;
		if (key != null)
			try {
				encryptedMessagePartBytes = DESEncryptionManager.encryptData(key, messagePartBytes);
			} catch (GeneralSecurityException gse) {
				throw new GeneralSecurityException("Exception happened while decrypting message");
			}

		Integer packetLength = PACKET_SIZE_NO_MESSAGE + encryptedMessagePartBytes.length;
		Integer packetPartFirstLength = Byte.BYTES + Byte.BYTES + Long.BYTES + Integer.BYTES;

		byte[] packetPartFirst = ByteBuffer.allocate(packetPartFirstLength)
			.put(bMagic)
			.put(bSrc)
			.putLong(bPktId.longValue())
			.putInt(packetLength)
			.array();

		wCrc16_1 = calculatedCrc16(packetPartFirst);
		Integer packetPartSecondLength = encryptedMessagePartBytes.length;
		byte[] packetPartSecond = ByteBuffer.allocate(packetPartSecondLength)
			.put(encryptedMessagePartBytes)
			.array();
		wCrc16_2 = calculatedCrc16(packetPartSecond);

		return ByteBuffer.allocate(packetLength)
			.put(packetPartFirst)
			.putShort(wCrc16_1)
			.put(packetPartSecond)
			.putShort(wCrc16_2)
			.array();
	}
}
