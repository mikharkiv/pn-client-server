package ua.org.edu.ukma.pncs.app.client;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ClientsRunner {
	private final static int THREADS_NUM = 10;

	public ClientsRunner(boolean tcp, int numClients) {
		ExecutorService pool = Executors.newFixedThreadPool(THREADS_NUM);
		for (int i = 0; i < numClients; i++) {
			if (tcp) {
				TestClient tc = new TestClient();
				pool.submit(tc);
			} else {
				TestUDPClient tuc = new TestUDPClient();
				pool.submit(tuc);
			}
		}
		try {
			pool.shutdown();
			pool.awaitTermination(3, TimeUnit.HOURS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("\n\nAll of clients finished\n");
	}
}
