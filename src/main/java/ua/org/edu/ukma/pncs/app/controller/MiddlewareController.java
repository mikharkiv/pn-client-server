package ua.org.edu.ukma.pncs.app.controller;

import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.controller.model.IMiddlewareController;
import ua.org.edu.ukma.pncs.app.controller.model.MiddlewareMessage;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.EventListener;
import ua.org.edu.ukma.pncs.app.network.model.AbstractUser;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;

public class MiddlewareController extends IMiddlewareController implements Runnable, EventListener {
	private final AbstractUser user;
	private final KeyExchangeMiddleware keyExchangeMiddleware;
	private final LoginMiddleware loginMiddleware;
	private final DataRequestMiddleware dataRequestMiddleware;

	public MiddlewareController(AbstractUser user, DBService database) {
		this.user = user;
		user.addEventListener(this);
		this.keyExchangeMiddleware = new KeyExchangeMiddleware();
		this.loginMiddleware = new LoginMiddleware(database);
		this.dataRequestMiddleware = new DataRequestMiddleware(database);
		keyExchangeMiddleware.setManager(this);
		keyExchangeMiddleware.setNext(loginMiddleware);
		loginMiddleware.setNext(dataRequestMiddleware);
		state = State.INITED;
	}

	@Override
	public void run() {
		if (state != State.INITED)
			return;
		state = State.RUNNING;
		while (state == State.RUNNING) {
			process(new MiddlewareMessage(user.nextPacket(), user));
		}
	}

	private void process(MiddlewareMessage p) {
		if (state == State.STOPPED) return;
		keyExchangeMiddleware.handle(p);
	}

	// Final point, if packet passed through all of middlewares
	// and didn't find their destination
	@Override
	public void handle(MiddlewareMessage p) {
		// todo send error message
	}

	@Override
	public void result(Packet p) {
		user.send(p);
	}

	@Override
	public void onException(Exception e) {
		e.printStackTrace();
	}

	@Override
	public void terminate() {
		state = State.STOPPED;
	}

	@Override
	public void onEvent(Event e) {
		process(new MiddlewareMessage(e, user));
	}
}
