package ua.org.edu.ukma.pncs.app.storage.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
	private Integer id;
	@EqualsAndHashCode.Include
	private String login;
	@EqualsAndHashCode.Include
	private String password;

	public User(String login, String password) {
		this.login = login;
		this.password = password;
		id = -1;
	}
}
