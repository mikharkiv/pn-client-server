package ua.org.edu.ukma.pncs.app.storage.dao;

import ua.org.edu.ukma.pncs.app.storage.dao.exception.TransactionException;

import java.util.List;
import java.util.Optional;

public interface Dao<T> {

	Optional<T> get(int id);

	List<T> getAll();

	void save(T t) throws TransactionException;

	void update(T t, Object... params) throws TransactionException;

	void delete(T t) throws TransactionException;

	void clear() throws TransactionException;
}
