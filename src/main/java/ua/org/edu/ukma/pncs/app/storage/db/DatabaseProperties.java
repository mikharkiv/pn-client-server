package ua.org.edu.ukma.pncs.app.storage.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DatabaseProperties {
	private static Properties props;
	private static final String configPath = Thread.currentThread().getContextClassLoader().getResource("database.properties").getPath();

	public static String getProperty(String key) {
		if (props == null) {
			props = new Properties();
			try {
				props.load(new FileInputStream(configPath));
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
		return props.getProperty(key);
	}
}
