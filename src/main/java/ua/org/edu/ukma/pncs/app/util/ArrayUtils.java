package ua.org.edu.ukma.pncs.app.util;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class ArrayUtils {
	/**
	 * 1024 bit symmetric key size is so big for DES so we must shorten the key size. You can get first 8 longKey of the
	 * byte array or can use a key factory
	 *
	 * @param longKey
	 * @return
	 */
	public static byte[] shortenSecretKey(final byte[] longKey) {
		try {
			final SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DESEncryptionManager.ALGORITHM);
			final DESKeySpec desSpec = new DESKeySpec(longKey);
			return keyFactory.generateSecret(desSpec).getEncoded();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
