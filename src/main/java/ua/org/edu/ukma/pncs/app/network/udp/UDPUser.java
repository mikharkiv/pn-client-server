package ua.org.edu.ukma.pncs.app.network.udp;

import com.google.common.primitives.UnsignedLong;
import lombok.Getter;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.exception.PacketValidatingException;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.model.AbstractUser;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.security.GeneralSecurityException;
import java.util.Deque;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class UDPUser extends AbstractUser implements Runnable {
	private static final long ANSWER_CHECK_PERIOD = 4000;
	private static final int MAX_TRY_COUNT = 250;

	public enum Role {SERVER, CLIENT}
	private final Role role;
	@Getter
	private final DatagramSocket s;

	@Getter
	private final InetAddress address;
	@Getter
	private final Integer port;
	private final LinkedBlockingQueue<DatagramPacket> packetsToProcess;

	// check if answered
	private final Timer checkUnansweredMessagesTimer;
	private long prevUnansweredCount;
	private int tryCount;

	public UDPUser(DatagramSocket socket, InetAddress inetAddress, int port) {
		this(socket, inetAddress, port, Role.SERVER);
	}

	public UDPUser(DatagramSocket socket, InetAddress inetAddress, int port, Role role) {
		super();
		if (role == Role.SERVER) inputCounter++;
		this.s = socket;
		this.address = inetAddress;
		this.port = port;
		this.state = States.CREATED;
		this.packetsToProcess = new LinkedBlockingQueue<>();
		this.role = role;
		checkUnansweredMessagesTimer = new Timer();
	}

	@Override
	public void run() {
		if (state != States.CREATED)
			return;
		state = States.LISTENING;
		checkUnansweredMessagesTimer.schedule(checkUnansweredMessagesTask, ANSWER_CHECK_PERIOD, ANSWER_CHECK_PERIOD);
		dispatchEvent(new Event(Events.CLIE_LISTENING_STARTED, this));
		while (state == States.LISTENING) {
			if (role == Role.SERVER) {
				DatagramPacket packet = null;
				try {
					packet = packetsToProcess.poll(3, TimeUnit.HOURS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (packet != null)
					processDatagramPacket(packet);
			} else {
				byte[] buf = new byte[Packet.MAX_PACKET_SIZE];
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				try {
					s.receive(packet);
					processDatagramPacket(packet);
				} catch (SocketException e) {
					// DO NOT REMOVE
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void send(Packet p, boolean notEnqueue) {
		try {
			if (state != States.LISTENING)
				throw new UnsupportedOperationException("Error: state is not listening");
			p = processPacketOutput(p, notEnqueue);

			byte[] packetBytes = p.toByteArray(key);
			DatagramPacket datagramPacket = new DatagramPacket(packetBytes, packetBytes.length, address, port);
			s.send(datagramPacket);
		} catch (IOException | GeneralSecurityException e) {
			e.printStackTrace();
			state = States.STOPPED;
			dispatchEvent(new Event(Events.CLIE_EXCEPTION, e, this));
		}
	}

	public void send(Packet p) {
		send(p, false);
	}

	private void processDatagramPacket(DatagramPacket datagram) {
		try {
			Packet dataPacket = new Packet(key, datagram.getData());

			Deque<Packet> temp = processPacketInput(dataPacket);
			if (temp != null && !temp.isEmpty())
				for (Packet packet1 : temp)
					send(packet1, true);
		} catch (UnsupportedOperationException | GeneralSecurityException | PacketValidatingException e) {
			System.err.println(e.getMessage());
			send(processPacketError(), true);
		}
	}

	public void putPacketToProcess(DatagramPacket p) {
		packetsToProcess.offer(p);
	}

	private final TimerTask checkUnansweredMessagesTask = new TimerTask() {
		@Override
		public void run() {
			checkUnansweredMessages();
		}
	};

	private void checkUnansweredMessages() {
		if (outputCounter > inputCounter) {
			System.out.printf("Resending %d  packets\n", outputCounter-inputCounter);
			if (prevUnansweredCount == outputCounter - inputCounter)
				tryCount++;
			if (tryCount >= MAX_TRY_COUNT) {
				checkUnansweredMessagesTimer.purge();
				System.out.println("Stop trying send more unanswered packets");
				disconnect();
			}
			prevUnansweredCount = outputCounter - inputCounter;
			for (int i = 0; i < outputCounter - inputCounter; ++i)
				send(history.get(UnsignedLong.valueOf(inputCounter + i)), true);
		}
	}

	@Override
	public void disconnect() {
		super.disconnect();
		if (role == Role.SERVER) {
			packetsToProcess.notifyAll();
		} else {
			s.close();
		}
		checkUnansweredMessagesTimer.cancel();
	}
}
