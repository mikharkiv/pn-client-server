package ua.org.edu.ukma.pncs.app.storage.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
	private Integer id;
	@EqualsAndHashCode.Include
	private Group group;
	@EqualsAndHashCode.Include
	private String name;
	private String manufacturer;
	private String description;
	private Integer quantity;
	private Double price;

	public Product(Group group, String name, Integer quantity, Double price) {
		this.group = group;
		this.name = name;
		this.quantity = quantity;
		this.price = price;
		manufacturer = "";
		description = "";
		id = -1;
	}

	public Product(Group group, String name,String manufacturer, String description, Integer quantity, Double price) {
		this.group = group;
		this.name = name;
		this.quantity = quantity;
		this.price = price;
		this.manufacturer = manufacturer;
		this.description = description;
		id = -1;
	}

	public String info(){
		String info = "Id: " + id + "\n" +
			"Group: " + group + "\n" +
			"Manufacturer: " + manufacturer + "\n" +
			"Description: " + description + "\n" +
			"Price: " + price + "\n" +
			"Quantitu: " + quantity;
		return info;
	}

	@Override
	public String toString(){
		return name;
	}

	@Deprecated
	public boolean remove(int quantity) {
		if (quantity < 0) return false;
		if (this.quantity >= quantity) {
			this.quantity -= quantity;
			return true;
		} else return false;
	}

	@Deprecated
	public boolean add(int quantity) {
		if (quantity < 0) return false;
		this.quantity += quantity;
		return true;
	}

	public boolean validate() {
		return quantity >= 0 && price >= 0;
	}

}
