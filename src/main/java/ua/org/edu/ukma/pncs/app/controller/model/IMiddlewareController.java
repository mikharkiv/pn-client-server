package ua.org.edu.ukma.pncs.app.controller.model;

import ua.org.edu.ukma.pncs.app.network.entity.Packet;

public abstract class IMiddlewareController {
	protected enum State {INITED, RUNNING, STOPPED}
	protected State state;

	public abstract void handle(MiddlewareMessage p);
	public abstract void result(Packet p);
	public abstract void onException(Exception e);
	public abstract void terminate();
}
