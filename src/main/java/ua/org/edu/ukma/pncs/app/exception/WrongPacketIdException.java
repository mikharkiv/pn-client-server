package ua.org.edu.ukma.pncs.app.exception;

public class WrongPacketIdException extends Exception {
	public WrongPacketIdException(String errorMessage) {
		super(errorMessage);
	}
}
