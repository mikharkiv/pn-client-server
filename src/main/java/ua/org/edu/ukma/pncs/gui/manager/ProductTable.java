package ua.org.edu.ukma.pncs.gui.manager;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Getter;
import lombok.Setter;
import ua.org.edu.ukma.pncs.app.client.ClientNetworkFacade;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ProductTable extends TableManager<Product> {

	@Getter
	@Setter
	private Group groupProperty = null;

	public ProductTable(TableView<Product> table) {
		super(table);
		update();
	}

	public void delete() {
		ClientNetworkFacade.removeProduct(table.getSelectionModel().getSelectedItem().getId());
	}

	protected void updateView() {
		table.getItems().remove(0, table.getItems().size());
		if (list != null)
			table.getItems().addAll(list);
		if (groupProperty != null) {
			List<Product> newList = list.stream().filter(product -> product.getGroup().equals(groupProperty)).collect(Collectors.toList());
			table.getItems().retainAll(newList);
		}
		if (!searchProperty.isEmpty()) {
			List<Product> newList = list.stream().filter(product -> product.getName().toLowerCase().contains(searchProperty.toLowerCase())).collect(Collectors.toList());
			table.getItems().retainAll(newList);
		}
	}

	protected void updateTable() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(ProductTable.products);
		list = ProductTable.products;
	}

	public static void initTable(TableView productTable) {
		TableColumn idColumn = new TableColumn("ID");
		idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

		TableColumn nameColumn = new TableColumn("Name");
		nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

		TableColumn groupColumn = new TableColumn("Group");
		groupColumn.setCellValueFactory(new PropertyValueFactory<>("group"));

		TableColumn manufacturerColumn = new TableColumn("Manufacturer");
		manufacturerColumn.setCellValueFactory(new PropertyValueFactory<>("manufacturer"));

		TableColumn priceColumn = new TableColumn("Price");
		priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));

		TableColumn quantityColumn = new TableColumn("Quantity");
		quantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));

		productTable.getColumns().addAll(idColumn, nameColumn, groupColumn, priceColumn, quantityColumn);
	}

	public static List<Product> products;

	public static void noProducts() {
		products = null;
	}

	public static void setProducts(Product[] products) {
		ProductTable.products = new ArrayList();
		Collections.addAll(ProductTable.products, products);
	}

	public static void addProduct(Product product) {
		if (ProductTable.products == null)
			ProductTable.products = new ArrayList();
		ProductTable.products.add(product);
	}

	public static void addProducts(Product[] products) {
		if (ProductTable.products == null)
			ProductTable.products = new ArrayList();
		Collections.addAll(ProductTable.products, products);
		System.out.println(ProductTable.products);
	}
}
