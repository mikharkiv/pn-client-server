package ua.org.edu.ukma.pncs.app.exception;

public class PacketValidatingException extends PacketException {
	public PacketValidatingException(String errorMessage) {
		super(errorMessage);
	}
}
