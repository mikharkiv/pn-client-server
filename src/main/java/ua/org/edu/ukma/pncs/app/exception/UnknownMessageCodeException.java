package ua.org.edu.ukma.pncs.app.exception;

public class UnknownMessageCodeException extends Exception {
	public UnknownMessageCodeException(String message) {
		super(message);
	}
}
