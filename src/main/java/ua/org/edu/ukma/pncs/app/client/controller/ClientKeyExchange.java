package ua.org.edu.ukma.pncs.app.client.controller;

import com.google.common.primitives.UnsignedLong;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.EventDispatcher;
import ua.org.edu.ukma.pncs.app.event.EventListener;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.entity.KeyExchangeMessage;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.network.tcp.TCPUser;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class ClientKeyExchange extends EventDispatcher implements EventListener {
	private TCPUser user;
	private KeyExchange keyExchange;

	public ClientKeyExchange(TCPUser user) {
		this.user = user;
		user.addEventListener(this);
		keyExchange = new KeyExchange();
	}

	@Override
	public void onEvent(Event e) {
		if (e.getEventType() == Events.CLIE_DATA_RECEIVED) {
			Packet packet = (Packet) e.getData()[0];
			MessageCodes msgType = MessageCodes.values()[packet.getMessage().getCType()];
			System.out.println(packet);

			if (keyExchange.getState() == KeyExchange.State.WAITING_FOR_PUBLIC_KEY && msgType == MessageCodes.KEY_EXCHANGE) {
				try {
					byte[] serverPubKey = ((KeyExchangeMessage) packet.getMessage()).getKey();
					keyExchange.processPublicKey(serverPubKey);
					user.send(new Packet((byte) 0, UnsignedLong.ZERO,
							new KeyExchangeMessage(MessageCodes.KEY_EXCHANGE.ordinal(), 0, keyExchange.getPublicKey())));
					user.setKey(keyExchange.getSharedSecret());
				} catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidAlgorithmParameterException | InvalidKeyException ex) {
					ex.printStackTrace();
				}
			} else if (keyExchange.getState() == KeyExchange.State.WAITING_FOR_APPROVAL && msgType == MessageCodes.CONNECTION_ESTABLISHED) {
				keyExchange.approveConnection(packet.getMessage());
				user.removeEventListener(this);
				dispatchEvent(new Event(Events.KEY_EXCHANGE_CONNECTION_ESTABLISHED));
			}

		}
	}
}
