package ua.org.edu.ukma.pncs.app.event;

import java.util.ArrayList;

public abstract class EventDispatcher {
	private final ArrayList<EventListener> listeners = new ArrayList<>();

	public void addEventListener(EventListener e) {
		listeners.add(e);
	}

	public void removeEventListener(EventListener e) {
		if (listeners.contains(e))
			listeners.remove(e);
		else
			throw new IllegalArgumentException("No such listener");
	}

	public void dispatchEvent(Event e) {
		// concurrency fix
		EventListener[] temp = listeners.toArray(new EventListener[listeners.size()]);
		for (EventListener listener : temp)
			listener.onEvent(e);
	}
}
