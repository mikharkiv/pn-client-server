package ua.org.edu.ukma.pncs.app.storage.dao;

import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.dao.exception.ErrorHandler;
import ua.org.edu.ukma.pncs.app.storage.dao.exception.TransactionException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GroupDao implements Dao<Group> {
	public static final String TABLE_NAME = "Groups";

	public static final String COLUMN_1_ID = "group_id";
	public static final String COLUMN_2_NAME = "group_name";
	public static final String COLUMN_3_DESCRIPTION = "group_description";

	public static final String CREATE_TABLE = "create table if not exists " + TABLE_NAME + " ( '" +
		COLUMN_1_ID + "' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '" +
		COLUMN_2_NAME + "' TEXT NOT NULL UNIQUE, '" +
		COLUMN_3_DESCRIPTION + "' TEXT " +
		");";

	public static final String INSERT_GROUP = "INSERT INTO " + TABLE_NAME + "(" + COLUMN_2_NAME + ", " + COLUMN_3_DESCRIPTION + ") VALUES (?, ?)";
	public static final String SELECT_GROUPS = "SELECT * FROM " + TABLE_NAME;

	public static final String DELETE_BY_NAME = "DELETE FROM " + TABLE_NAME + " WHERE " + COLUMN_2_NAME + "=?";
	public static final String DELETE_BY_ID = "DELETE FROM " + TABLE_NAME + " WHERE " + COLUMN_1_ID + "=?";

	public static final String GET_BY_NAME = "SELECT * FROM " + TABLE_NAME + "  WHERE " + COLUMN_2_NAME + "=?";
	public static final String GET_BY_ID = "SELECT * FROM " + TABLE_NAME + "  WHERE " + COLUMN_1_ID + "=?";

	public static final String UPDATE_BY_NAME = "UPDATE " + TABLE_NAME + " SET " + COLUMN_2_NAME + "=?, " + COLUMN_3_DESCRIPTION + "=? WHERE " + COLUMN_2_NAME + "=?";
	public static final String UPDATE_BY_ID = "UPDATE " + TABLE_NAME + " SET " + COLUMN_2_NAME + "=?, " + COLUMN_3_DESCRIPTION + "=? WHERE " + COLUMN_1_ID + "=?";

	private final Connection connection;

	public GroupDao(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @param id
	 * @return Group from the table by id
	 */
	@Override
	public Optional<Group> get(int id) {
		try {
			PreparedStatement st = connection.prepareStatement(GET_BY_ID);
			st.setLong(1, id);
			ResultSet res = st.executeQuery();
			Group resultGroup = null;
			if (res.next()) {
				resultGroup = new Group(res.getInt(COLUMN_1_ID), res.getString(COLUMN_2_NAME), res.getString(COLUMN_3_DESCRIPTION));
			}
			res.close();
			st.close();
			return Optional.ofNullable(resultGroup);
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			return Optional.empty();
		}
	}

	/**
	 * @return All groups from the table
	 */
	@Override
	public List<Group> getAll() {
		try {
			PreparedStatement st = connection.prepareStatement(SELECT_GROUPS);

			ResultSet res = st.executeQuery();
			ArrayList<Group> resultList = new ArrayList<>();
			while (res.next()) {
				resultList.add(new Group(res.getInt(COLUMN_1_ID), res.getString(COLUMN_2_NAME), res.getString(COLUMN_3_DESCRIPTION)));
			}
			res.close();
			st.close();
			return resultList;
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			return null;
		}
	}

	/**
	 * Save group
	 *
	 * @param group
	 * @throws TransactionException
	 */
	@Override
	public void save(Group group) throws TransactionException {
		try {
			PreparedStatement statement = connection.prepareStatement(INSERT_GROUP);
			statement.setString(1, group.getName());
			statement.setString(2, group.getDescription());
			int result = statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * Replace group
	 *
	 * @param group
	 * @param params
	 */
	@Override
	public void update(Group group, Object... params) throws TransactionException {
		try {
			if (params.length < 1) return;
			PreparedStatement statement = connection.prepareStatement(UPDATE_BY_NAME);
			statement.setString(3, group.getName());
			statement.setString(1, params[0] == null ? group.getName() : (String) params[0]);
			if (params.length > 1)
				statement.setString(2, params[1] == null ? group.getDescription() : (String) params[1]);
			else
				statement.setString(2, group.getDescription());
			int result = statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * Replace group
	 *
	 * @param group
	 */
	public void update(Group group) throws TransactionException {
		try {
			PreparedStatement statement = connection.prepareStatement(UPDATE_BY_ID);
			statement.setInt(3, group.getId());
			statement.setString(1, group.getName());
			statement.setString(2, group.getDescription());
			int result = statement.executeUpdate();
			statement.close();
			if (result < 0) throw new TransactionException("Transaction failed");
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * Delete group from the table
	 *
	 * @param group
	 * @throws TransactionException
	 */
	@Override
	public void delete(Group group) throws TransactionException {
		try {
			PreparedStatement statement = connection.prepareStatement(DELETE_BY_NAME);
			statement.setString(1, group.getName());
			int result = statement.executeUpdate();
			if (result <= 0) throw new TransactionException("Group has not been deleted");
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * Delete group from the table
	 *
	 * @param id
	 * @throws TransactionException
	 */
	public void delete(int id) throws TransactionException {
		try {
			PreparedStatement statement = connection.prepareStatement(DELETE_BY_ID);
			statement.setInt(1, id);
			int result = statement.executeUpdate();
			if (result <= 0) throw new TransactionException("Group has not been deleted");
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * Clear the table
	 *
	 * @throws TransactionException
	 */
	@Override
	public void clear() throws TransactionException {
		try {
			Statement statement = connection.createStatement();
			int result = statement.executeUpdate("delete from " + TABLE_NAME);
			statement.close();
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			throw new TransactionException("Transaction failed");
		}
	}

	/**
	 * @param name
	 * @return Group from the table by name
	 */
	public Optional<Group> get(String name) {
		try {
			PreparedStatement st = connection.prepareStatement(GET_BY_NAME);
			st.setString(1, name);
			ResultSet res = st.executeQuery();
			Group resultGroup = null;
			if (res.next()) {
				resultGroup = new Group(res.getInt(COLUMN_1_ID), res.getString(COLUMN_2_NAME), res.getString(COLUMN_3_DESCRIPTION));
			}
			res.close();
			st.close();
			return Optional.ofNullable(resultGroup);
		} catch (SQLException e) {
			ErrorHandler.handleSQLException(e);
			return Optional.empty();
		}
	}
}
