package ua.org.edu.ukma.pncs.gui.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ua.org.edu.ukma.pncs.gui.animations.LoadingAnimation;
import ua.org.edu.ukma.pncs.gui.utils.AlertPane;
import ua.org.edu.ukma.pncs.gui.utils.Validator;
import ua.org.edu.ukma.pncs.app.client.ClientNetworkFacade;
import ua.org.edu.ukma.pncs.app.storage.model.User;

import java.awt.*;

public class EditUserController {
	@FXML
	public PasswordField passwordField;
	@FXML
	public TextField loginField;
	@FXML
	public Button cancelButton;
	@FXML
	public Button saveButton;
	@FXML
	public Group loadingGroup;
	@FXML
	public Label loginMsg;
	@FXML
	public Label passwordMsg;

	LoadingAnimation loadingAnimation;

	volatile private static User editUser;

	private static boolean isError = false;

	private static String serverError = "error";

	@FXML
	public void initialize() {
		loadingAnimation = new LoadingAnimation(loadingGroup);
		Validator.setUpTextValidation(loginField, loginMsg);
		Validator.setUpPasswordValidation(passwordField, passwordMsg);
		update();
	}

	public void onCancelAction(ActionEvent actionEvent) {
		closeStage(actionEvent);
	}

	public void onSaveAction(ActionEvent actionEvent) {
		if (loginField.getText().equals("") || passwordField.getText().length() < 4) {
			Toolkit.getDefaultToolkit().beep();
			return;
		}

		new Thread(() -> {
			Platform.runLater(() -> {
				loadingStart();
			});

			tryToSave();

			Platform.runLater(() -> {
				loadingStop();
				if (editUser != null) {
					AlertPane.showInfo("Success", "User data has been changed");
					StorageController.currentUser = editUser;
					update();
				} else
					AlertPane.showError("Error", "User data has not been changed");
			});
		}).start();
	}

	private void tryToSave() {
		String login = loginField.getText();
		String pass = passwordField.getText();

		int timeout = 0;

		System.out.println(login + "  " + pass);

		ClientNetworkFacade.updateUser(new User(StorageController.currentUser.getId(), login, pass));

		while (editUser == null) {
			try {
				Thread.sleep(100);
				timeout += 100;
				if (timeout > 5000) return;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void loadingStart() {
		saveButton.setVisible(false);
		cancelButton.setVisible(false);
		loginField.setEditable(false);
		passwordField.setEditable(false);
		loadingAnimation.loadingAnimationStart();
	}

	private void loadingStop() {
		loadingAnimation.loadingAnimationStop();
		saveButton.setVisible(true);
		cancelButton.setVisible(true);
		loginField.setEditable(true);
		passwordField.setEditable(true);
	}

	private void closeStage(ActionEvent event) {
		Node source = (Node) event.getSource();
		Stage stage = (Stage) source.getScene().getWindow();
		stage.close();
	}

	public void setupUser() {
		update();
	}

	private void update() {
		editUser = null;
		loginField.setText(StorageController.currentUser.getLogin());
		passwordField.setText("");
	}

	public static void receiveUser(User user) {
		editUser = user;
	}

	public static void serverError(String error) {
		isError = true;
		serverError = error;
	}
}
