package ua.org.edu.ukma.pncs.app.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.primitives.UnsignedLong;
import ua.org.edu.ukma.pncs.gui.controllers.*;
import ua.org.edu.ukma.pncs.gui.manager.GroupTable;
import ua.org.edu.ukma.pncs.gui.manager.ProductTable;
import ua.org.edu.ukma.pncs.app.client.controller.ClientKeyExchange;
import ua.org.edu.ukma.pncs.app.event.Event;
import ua.org.edu.ukma.pncs.app.event.Events;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.network.tcp.TCPUser;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;
import ua.org.edu.ukma.pncs.app.storage.model.User;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.LinkedList;

public class ClientNetworkFacade {
	private static TCPUser connection;
	private static Thread t;

	private static boolean isConnectionEstablished;
	private static LinkedList<Packet> beforeConnEstablishedList;

	static {
		isConnectionEstablished = false;
		beforeConnEstablishedList = new LinkedList<>();
	}

	public static TCPUser connect(String ip, int port) {
		try {
			Socket socket = new Socket();
			socket.connect(new InetSocketAddress(ip, port));
			connection = new TCPUser(socket);
			ClientKeyExchange cke = new ClientKeyExchange(connection);
			cke.addEventListener(ClientNetworkFacade::onEvent);
			t = new Thread(connection);
			t.start();
			connection.addEventListener(ClientNetworkFacade::onEvent);
			return connection;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void login(String username, String password) {
		send(new Message(MessageCodes.LOGIN_DO_LOGIN.ordinal(), 0, username.concat(Message.DELIM).concat(password)));
	}

	// group

	public static void getGroups() {
		send(new Message(MessageCodes.GROUP_GET_GROUPS.ordinal(), 0, ""));
	}

	// price of all products in group
	public static void getGroupInfo(Integer id) {
		send(new Message(MessageCodes.GROUP_GET_GROUP_DETAILS.ordinal(), 0, String.valueOf(id)));
	}

	public static void getGroupProducts(Integer id) {
		send(new Message(MessageCodes.GROUP_GET_PRODUCTS.ordinal(), 0, String.valueOf(id)));
	}


	public static void addGroup(Group group) {
		try {
			send(new Message(MessageCodes.GROUP_CREATE.ordinal(), 0, new ObjectMapper().writeValueAsString(group)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void removeGroup(Integer id) {
		send(new Message(MessageCodes.GROUP_REMOVE.ordinal(), 0, String.valueOf(id)));
	}

	public static void updateGroup(Group group) {
		try {
			send(new Message(MessageCodes.GROUP_UPDATE.ordinal(), 0, new ObjectMapper().writeValueAsString(group)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	// product

	public static void getProduct(Integer productId) {
		send(new Message(MessageCodes.PRODUCT_GET.ordinal(), 0, String.valueOf(productId)));
	}

	public static void updateProduct(Product product) {
		try {
			send(new Message(MessageCodes.PRODUCT_UPDATE.ordinal(), 0,
					new ObjectMapper().writeValueAsString(product)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void removeProduct(Integer id) {
		send(new Message(MessageCodes.PRODUCT_REMOVE.ordinal(), 0, String.valueOf(id)));
	}

	public static void addProduct(Product product) {
		try {
			send(new Message(MessageCodes.PRODUCT_CREATE.ordinal(), 0, new ObjectMapper().writeValueAsString(product)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void updateUser(User user) {
		try {
			send(new Message(MessageCodes.USER_UPDATE.ordinal(), 0, new ObjectMapper().writeValueAsString(user)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	private static void send(Message m) {
		Packet p = new Packet((byte)0, UnsignedLong.ZERO, m);
		if (!isConnectionEstablished)
			beforeConnEstablishedList.add(p);
		else
			connection.send(p);
	}

	private static void onEvent(Event e) {
		if (e.getEventType() == Events.KEY_EXCHANGE_CONNECTION_ESTABLISHED) {
			isConnectionEstablished = true;
			AuthorizationController.serverResponse(MessageCodes.CONNECTION_ESTABLISHED);
			for (Packet packet : beforeConnEstablishedList)
				send(packet.getMessage());
		}  else if (e.getEventType() == Events.CLIE_DATA_RECEIVED) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				Message m = ((Packet) e.getData()[0]).getMessage();
				switch (MessageCodes.values()[m.getCType()]) {
					// LOGIN
					case LOGIN_SUCCESS:
						System.out.println("Login success!");
						User user = mapper.readValue(m.getMessage(), User.class); // WARNING: PASSWORD-NULL!!!
						StorageController.receiveUser(user);
						AuthorizationController.serverResponse(MessageCodes.LOGIN_SUCCESS);
						break;
					case LOGIN_FAIL:
						System.out.println("Login failed!");
						AuthorizationController.serverResponse(MessageCodes.LOGIN_FAIL);
						break;
					case LOGIN_NOT_LOGGED:
						System.out.println("Not logged!");
						AuthorizationController.serverResponse(MessageCodes.LOGIN_NOT_LOGGED);
						break;

					// GROUP
					case GROUP_GET_GROUPS:
						Group[] groups = mapper.readValue(m.getMessage(), Group[].class);
						GroupTable.setGroups(groups);
						break;
					case GROUP_GET_GROUP_DETAILS:
						if (m.getMessage().equals(String.valueOf(DBService.Result.FAILED.ordinal()))) {
							System.out.println("No such group!");

							break;
						}
						Integer totalProducts = Integer.parseInt(m.getArgs()[0]);
						Double totalPrice = Double.parseDouble(m.getArgs()[1]);

						break;
					case GROUP_GET_PRODUCTS:
						if (m.getMessage().equals(String.valueOf(DBService.Result.FAILED.ordinal()))) {
							System.out.println("No such group!");
							break;
						}
						Product[] products = mapper.readValue(m.getMessage(), Product[].class);
						ProductTable.addProducts(products);
						break;
					case GROUP_REMOVE:
						if (m.getMessage().equals(String.valueOf(DBService.Result.FAILED.ordinal()))) {
							System.out.println("No such group!");
						} else {
							Integer removedGroupId = Integer.parseInt(m.getMessage().substring(1));
							System.out.println("Group removing successful! Removed ID: ".concat(String.valueOf(removedGroupId)));
						}
						break;
					case GROUP_CREATE:
						if (m.getMessage().equals(String.valueOf(DBService.Result.FAILED.ordinal()))) {
							System.out.println("Group already exists!");
							AddGroupDialogController.serverError("Group already exists!");
						} else {
							Group createdGroup = mapper.readValue(m.getMessage(), Group.class);
							System.out.println("Group created! Result : ".concat(createdGroup.toString()));
							AddGroupDialogController.receiveGroup(createdGroup);
						}
						break;
					case GROUP_UPDATE:
						if (m.getMessage().equals(String.valueOf(DBService.Result.FAILED.ordinal()))) {
							System.out.println("No such group!");
							AddGroupDialogController.serverError("No such group!");
						} else {
							Group updatedGroup = mapper.readValue(m.getMessage(), Group.class);
							System.out.println("Group updated! Result : ".concat(updatedGroup.toString()));
							AddGroupDialogController.receiveGroup(updatedGroup);
						}

						break;

					// PRODUCT
					case PRODUCT_GET:
						if (m.getMessage().equals(String.valueOf(DBService.Result.FAILED.ordinal()))) {
							System.out.println("No such product!");

						} else {
							Product result = mapper.readValue(m.getMessage(), Product.class);
							System.out.println("Product get! Result : ".concat(result.toString()));

						}
						break;
					case PRODUCT_CREATE:
						if (m.getMessage().equals(String.valueOf(DBService.Result.FAILED.ordinal()))) {
							System.out.println("No such product!");
							AddProductDialogController.serverError("No such product!");
						} else {
							Product createdProduct = mapper.readValue(m.getMessage(), Product.class);
							System.out.println("Product created! Result : ".concat(createdProduct.toString()));
							AddProductDialogController.receiveProduct(createdProduct);
						}
						break;
					case PRODUCT_REMOVE:
						if (m.getMessage().equals(String.valueOf(DBService.Result.FAILED.ordinal()))) {
							System.out.println("No such product!");

						} else {
							Integer removedProductId = Integer.parseInt(m.getMessage().substring(1));
							System.out.println("Product removing successful! Removed ID: ".concat(String.valueOf(removedProductId)));

						}
						break;
					case PRODUCT_UPDATE:
						if (m.getMessage().equals(String.valueOf(DBService.Result.FAILED.ordinal()))) {
							System.out.println("Product already exists!");
							AddProductDialogController.serverError("Product already exists!");
						} else {
							Product updatedProduct = mapper.readValue(m.getMessage(), Product.class);
							System.out.println(m.getMessage());
							PlustSubtractDialogController.receiveProduct(updatedProduct);
							AddProductDialogController.receiveProduct(updatedProduct);
						}
						break;
					case USER_UPDATE:
						if (m.getMessage().equals(String.valueOf(DBService.Result.FAILED.ordinal()))) {
							System.out.println("No such user!");
							EditUserController.serverError("No such user!");
						} else {
							User updatedUser = mapper.readValue(m.getMessage(), User.class);
							System.out.println("User updated successfuly! Result : ".concat(updatedUser.toString()));
							EditUserController.receiveUser(updatedUser);
						}
				}
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}
}
