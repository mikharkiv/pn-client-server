package ua.org.edu.ukma.pncs.app;

import java.io.IOException;

public class TCPServerRunner {
	public static void main(String[] args) throws IOException {
		ServerRunner serverRunner = new ServerRunner(true);
		serverRunner.run();
	}
}
