package ua.org.edu.ukma.pncs.app.storage.db;

import ua.org.edu.ukma.pncs.app.storage.dao.UserDao;
import ua.org.edu.ukma.pncs.app.storage.dao.exception.TransactionException;
import ua.org.edu.ukma.pncs.app.storage.model.User;
import ua.org.edu.ukma.pncs.app.storage.model.Utils;

import java.sql.Connection;
import java.util.List;

public class UserService {
	private final UserDao userDao;

	public UserService(Connection connection) {
		this.userDao = new UserDao(connection);
	}

	/**
	 * Method for testing purposes
	 *
	 * @return Product
	 */
	public List<User> getAll() {
		return userDao.getAll();
	}

	/**
	 * Method for testing purposes
	 *
	 * @return
	 */
	synchronized public void clear() {
		try {
			userDao.clear();
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
		}
	}

	public User getUser(String login, String password) {
		return userDao.get(login, password).orElseGet(
			() -> Utils.emptyUser());
	}

	synchronized public DBService.Result createUser(User user) {
		try {
			userDao.save(user);
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

	synchronized public DBService.Result createUser(String login, String password) {
		return createUser(new User(login, password));
	}

	synchronized public DBService.Result deleteUser(User user) {
		try {
			userDao.delete(user);
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

	synchronized public DBService.Result deleteUser(String login, String password) {
		return deleteUser(new User(login, password));
	}

	synchronized public DBService.Result updateUser(User user) {
		try {
			userDao.updateUserPlease(user);
			return DBService.Result.SUCCESS;
		} catch (TransactionException te) {
			System.out.println(te.getMessage());
			return DBService.Result.FAILED;
		}
	}

}
