package ua.org.edu.ukma.pncs.lab4.dbMultithreadedTests;

import com.google.code.tempusfugit.concurrency.ConcurrentRule;
import com.google.code.tempusfugit.concurrency.RepeatingRule;
import com.google.code.tempusfugit.concurrency.annotations.Concurrent;
import com.google.code.tempusfugit.concurrency.annotations.Repeating;
import edu.umd.cs.mtc.MultithreadedTestCase;
import org.junit.*;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;
import ua.org.edu.ukma.pncs.app.storage.db.DBHelper;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;

public class SubtractProductTest extends MultithreadedTestCase {
	@Rule
	public ConcurrentRule concurrently = new ConcurrentRule();
	@Rule
	public RepeatingRule rule = new RepeatingRule();

	private static DBService service;

	private static final int initialQuantity = 5550;

	private static final Group testGroup = new Group("Wood");
	private static final Product testProd = new Product(testGroup, "Log", initialQuantity, 10.);

	private static final int subtract = 10;

	private static final int testCount = 10;
	private static final int testRepetition = 10;

	@BeforeClass
	public static void setup() {
		DBHelper.isTest = true;
		DBHelper.dropTables = true;
		service = DBService.instantiateService(DBHelper.getConnection());
		init();
	}

	private static void init() {
		service.clear();
		service.getGroupService().createGroup(testGroup);
		service.getProductService().createProduct(testProd);
		System.out.println("Initialized: " + service.getProductsToGroups());
	}

	@Test
	@Concurrent(count = testCount)
	@Repeating(repetition = testRepetition)
	public void runsMultipleTimes() {
		Assert.assertSame(service.getProductService().subtractProducts(testProd.getGroup().getName(), testProd.getName(), subtract), DBService.Result.SUCCESS);
	}

	@AfterClass
	public static void annotatedTestRunsMultipleTimes() {
		assertEquals(service.getProductService().howMany(testProd.getGroup().getName(), testProd.getName()).intValue(), initialQuantity - subtract*testCount*testRepetition);
	}
}
