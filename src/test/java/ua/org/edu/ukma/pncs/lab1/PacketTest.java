package ua.org.edu.ukma.pncs.lab1;

import com.google.common.primitives.UnsignedLong;
import org.junit.Assert;
import org.junit.Test;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;

public class PacketTest {

	public static final byte[] KEY = {31, 99, -8, 21, -39, -47, -73, 29};

	@Test
	public void test01MaxBPktIdValue() {
		UnsignedLong moreThanLongbPktId = UnsignedLong.valueOf(Long.MAX_VALUE);
		moreThanLongbPktId = moreThanLongbPktId.plus(UnsignedLong.valueOf("2305"));

		System.out.println("UnsignedLong moreThanLongbPktId: " + moreThanLongbPktId.toString());
		System.out.println("long moreThanLongbPktId: " + moreThanLongbPktId.longValue());

		Message msg = new Message(333,123,"Test Message ;)");
		Packet testPacket = new Packet((byte)111,moreThanLongbPktId,msg);

		System.out.println("Out packet: " + testPacket);
		System.out.println("Out packet length: " + testPacket.getWLen());

		try {
			byte[] encodedPacket = testPacket.toByteArray(KEY);
			System.out.println("Encoded packet length: " + encodedPacket.length);
			Packet decodedPacket = new Packet(KEY,encodedPacket);

			System.out.println("In packet: " + decodedPacket);

			Assert.assertEquals(testPacket, decodedPacket);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
