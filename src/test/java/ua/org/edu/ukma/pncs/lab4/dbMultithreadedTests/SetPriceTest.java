package ua.org.edu.ukma.pncs.lab4.dbMultithreadedTests;

import edu.umd.cs.mtc.MultithreadedTestCase;
import edu.umd.cs.mtc.TestFramework;
import org.junit.Test;
import ua.org.edu.ukma.pncs.app.storage.db.DBHelper;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;

public class SetPriceTest extends MultithreadedTestCase {
	private static DBService service;

	private final static String testGroup = "Vegetables";
	private final static String testProd = "Lettuce";

	private final static double expectedPrice = 10.54;

	@Override
	public void initialize() {
		DBHelper.isTest = true;
		DBHelper.dropTables = true;
		service = DBService.instantiateService(DBHelper.getConnection());
		init();
	}

	private void init() {
		service.clear();
		service.getGroupService().createGroup(testGroup);
		service.getProductService().createProduct(testProd, testGroup);
		System.out.println("Initialized: " + service.getProductsToGroups());
	}

	public void thread1() {
		assertSame(service.getProductService().setPrice(testGroup, testProd, expectedPrice), DBService.Result.SUCCESS);
	}

	public void thread2() {
		assertSame(service.getProductService().setPrice(testGroup, testProd, expectedPrice), DBService.Result.SUCCESS);
	}

	public void thread3() {
		assertSame(service.getProductService().setPrice(testGroup, testProd, -10.), DBService.Result.NEGATIVE_QUANTITY);
	}

	public void thread5() {
		assertNotSame(service.getProductService().setPrice("", "", 100.), DBService.Result.FAILED);
	}

	@Override
	public void finish() {
		assertEquals(expectedPrice, service.getProductService().getProduct(testGroup, testProd).getPrice());
	}

	@Test
	public void setPriceTest() throws Throwable {
		TestFramework.runManyTimes(new SetPriceTest(), 10);
	}
}
