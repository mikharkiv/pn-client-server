package ua.org.edu.ukma.pncs.lab4.dbMultithreadedTests;

import edu.umd.cs.mtc.MultithreadedTestCase;
import edu.umd.cs.mtc.TestFramework;
import org.junit.Test;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;
import ua.org.edu.ukma.pncs.app.storage.db.DBHelper;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;

public class AddProductToGroupTest extends MultithreadedTestCase {
	private static DBService service;

	@Override
	public void initialize() {
		DBHelper.isTest = true;
		DBHelper.dropTables = true;
		service = DBService.instantiateService(DBHelper.getConnection());
		init();
	}

	private void init() {
		service.clear();
		service.getGroupService().createGroup("Fruit");
		service.getGroupService().createGroup("FruitA");
		service.getGroupService().createGroup("FruitB");
		service.getGroupService().createGroup("FruitC");

		service.getProductService().createProduct("Some Product", "Fruits");

		System.out.println("Initialized: " + service.getProductsToGroups());
	}

	public void thread1() {
		service.getProductService().createProduct("Banana", "Fruit");
	}

	public void thread2() {
		service.getProductService().createProduct("Banana", "Fruit");
	}

	public void thread3() {
		service.getProductService().deleteProduct("Some Product", "Fruit");
	}

	public void thread4() {
		service.getProductService().deleteProduct("Some Product", "Fruit");
	}

	public void thread5() {
		assertSame(service.getProductService().createProduct("MelonA", "FruitA"), DBService.Result.SUCCESS);
	}

	public void thread6() {
		assertSame(service.getProductService().createProduct("MelonB", "FruitB"), DBService.Result.SUCCESS);
	}

	public void thread7() {
		assertSame(service.getProductService().createProduct("MelonC", "FruitC"), DBService.Result.SUCCESS);
	}

	public void thread8() {
		assertSame(service.getProductService().createProduct("MelonD", "FruitD"), DBService.Result.FAILED);
	}

	@Test
	public void addProductToGroup() throws Throwable {
		TestFramework.runManyTimes(new AddProductToGroupTest(), 1);
		assertEquals(service.getProductService().getProduct("Fruit", "Some Product"), new Product(new Group(""), "", 0, 0d));
		assertEquals(service.getProductService().getProduct("Fruit", "Banana"), new Product(new Group("Fruit"), "Banana", 0, 0d));
		assertTrue(service.getProductsToGroup("FruitA").getValue().contains(new Product(new Group("FruitA"), "MelonA", 0, 0d)));
		assertTrue(service.getProductsToGroup("FruitB").getValue().contains(new Product(new Group("FruitB"), "MelonB", 0, 0d)));
		assertTrue(service.getProductsToGroup("FruitC").getValue().contains(new Product(new Group("FruitC"), "MelonC", 0, 0d)));
		System.out.println(service.getProductsToGroups());
	}
}

