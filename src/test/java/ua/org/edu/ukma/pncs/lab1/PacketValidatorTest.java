package ua.org.edu.ukma.pncs.lab1;

import com.google.common.primitives.UnsignedLong;
import org.junit.Test;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.network.entity.Packet;
import ua.org.edu.ukma.pncs.app.exception.PacketValidatingException;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

public class PacketValidatorTest {
	public static final byte[] KEY = {31, 99, -8, 21, -39, -47, -73, 29};

	private static byte[] getSomePacket() throws UnsupportedEncodingException, GeneralSecurityException {
		Message message = new Message(0, 1, "KMA");
		Packet packet = new Packet(Byte.MAX_VALUE, UnsignedLong.MAX_VALUE, message);
		return packet.toByteArray(KEY);
	}

	@Test(expected = PacketValidatingException.class)
	public void testPacketValidationFailMagic() throws UnsupportedEncodingException, GeneralSecurityException, PacketValidatingException {
		byte[] bytes = getSomePacket();
		bytes[0] = 0x2;
		new Packet(KEY, bytes);
	}

	@Test(expected = PacketValidatingException.class)
	public void testPacketValidationFailHeaderChecksum() throws UnsupportedEncodingException, GeneralSecurityException, PacketValidatingException {
		byte[] bytes = getSomePacket();
		bytes[Packet.HEADER_SIZE_NO_CRC + 1] = 0xF;
		new Packet(KEY, bytes);
	}

	@Test(expected = PacketValidatingException.class)
	public void testPacketValidationFailMessageChecksum() throws UnsupportedEncodingException, GeneralSecurityException, PacketValidatingException {
		byte[] bytes = getSomePacket();
		bytes[bytes.length - 1] = 0xF;
		new Packet(KEY, bytes);
	}

		@Test
	public void testPacketValidationSuccess() throws UnsupportedEncodingException, GeneralSecurityException, PacketValidatingException {
		Message message = new Message(0, 1, "KMA");
		Packet packet = new Packet(Byte.MAX_VALUE, UnsignedLong.MAX_VALUE, message);
		byte[] bytes = packet.toByteArray(KEY);
		new Packet(KEY, bytes);
	}
}
