package ua.org.edu.ukma.pncs.lab4.dbMultithreadedTests;

import edu.umd.cs.mtc.MultithreadedTestCase;
import edu.umd.cs.mtc.TestFramework;
import org.junit.Test;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.db.DBHelper;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;
import ua.org.edu.ukma.pncs.app.storage.db.GroupService;

public class AddGroupTest extends MultithreadedTestCase {
	private static GroupService service;

	private final static String addGroupTest = "Vegetables";
	private final static String deleteGroupTest = "F Group";

	@Override
	public void initialize() {
		DBHelper.isTest = true;
		DBHelper.dropTables = true;
		service = DBService.instantiateService(DBHelper.getConnection()).getGroupService();
		init();
	}

	private void init() {
		service.clear();
		service.createGroup(deleteGroupTest);
	}

	public void thread1() {
		service.createGroup(addGroupTest);
	}

	public void thread2() {
		service.createGroup(addGroupTest);
	}

	public void thread3() {
		service.deleteGroup(deleteGroupTest);
	}

	public void thread4() {
		service.deleteGroup(deleteGroupTest);
	}

	public void thread5() {
		assertSame(service.createGroup("Minerals"), DBService.Result.SUCCESS);
	}

	public void thread6() {
		assertSame(service.createGroup("Fruit"), DBService.Result.SUCCESS);
	}

	@Test
	public void addGroupTest() throws Throwable {
		TestFramework.runManyTimes(new AddGroupTest(), 1);
		assertEquals(service.getGroup(deleteGroupTest), new Group(""));
		assertEquals(service.getGroup(addGroupTest), new Group(addGroupTest));
		assertEquals(service.getGroup("Minerals"), new Group("Minerals"));
		assertEquals(service.getGroup("Fruit"), new Group("Fruit"));
	}
}
