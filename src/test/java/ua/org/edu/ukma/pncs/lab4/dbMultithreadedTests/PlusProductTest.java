package ua.org.edu.ukma.pncs.lab4.dbMultithreadedTests;

import com.google.code.tempusfugit.concurrency.ConcurrentRule;
import com.google.code.tempusfugit.concurrency.RepeatingRule;
import com.google.code.tempusfugit.concurrency.annotations.Concurrent;
import com.google.code.tempusfugit.concurrency.annotations.Repeating;
import edu.umd.cs.mtc.MultithreadedTestCase;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;
import ua.org.edu.ukma.pncs.app.storage.db.DBHelper;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;

public class PlusProductTest extends MultithreadedTestCase {
	@Rule
	public ConcurrentRule concurrently = new ConcurrentRule();
	@Rule
	public RepeatingRule rule = new RepeatingRule();

	private static DBService service;

	private static final Group testGroup = new Group("Wood");
	private static final Product testProd = new Product(testGroup, "Log", 0, 10.);

	private final static int plus = 10;

	private final static int testCount = 10;
	private final static int testRepetition = 10;

	@BeforeClass
	public static void setup() {
		DBHelper.isTest = true;
		DBHelper.dropTables = true;
		service = DBService.instantiateService(DBHelper.getConnection());
		init();
	}

	private static void init() {
		service.clear();
		service.getGroupService().createGroup(testGroup);
		service.getProductService().createProduct(testProd);
		System.out.println("Initialized: " + service.getProductsToGroups());
	}

	@Test
	@Concurrent(count = testCount)
	@Repeating(repetition = testRepetition)
	public void runsMultipleTimes() {
		service.getProductService().plusProducts(testProd.getGroup().getName(), testProd.getName(), plus);
	}

	@AfterClass
	public static void annotatedTestRunsMultipleTimes() {
		assertEquals(service.getProductService().howMany(testProd.getGroup().getName(), testProd.getName()).intValue(), plus * testCount * testRepetition);
	}
}
