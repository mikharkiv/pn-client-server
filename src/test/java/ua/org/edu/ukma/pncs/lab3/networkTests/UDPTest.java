package ua.org.edu.ukma.pncs.lab3.networkTests;

import edu.umd.cs.mtc.MultithreadedTestCase;
import edu.umd.cs.mtc.TestFramework;
import javafx.util.Pair;
import org.junit.Test;
import ua.org.edu.ukma.pncs.app.ServerRunner;
import ua.org.edu.ukma.pncs.app.client.TestUDPClient;
import ua.org.edu.ukma.pncs.app.network.entity.MessageCodes;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class UDPTest extends MultithreadedTestCase {

	ServerRunner serverRunner;

	@Override
	public void initialize() {
		runUDPServer();
	}

	private void runUDPServer() {
		try {
			serverRunner = new ServerRunner(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
		serverRunner.run();
	}

	private void runUDPClient(List<Pair<MessageCodes, String>> messages) {
		try {
			TestUDPClient client = new TestUDPClient(messages);
			Thread clientThread = new Thread(client);
			clientThread.start();
			clientThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void thread1() {
		final List<Pair<MessageCodes, String>> testMessages = Arrays.asList(
			new Pair<>(MessageCodes.ADD_PRODUCTS, "Fruit$Apples$2000"));
		runUDPClient(testMessages);
	}

	public void thread2() {
		final List<Pair<MessageCodes, String>> testMessages = Arrays.asList(
			new Pair<>(MessageCodes.ADD_PRODUCTS, "Fruit$Apples$2000"));
		runUDPClient(testMessages);
	}

	public void thread3() {
		final List<Pair<MessageCodes, String>> testMessages = Arrays.asList(
			new Pair<>(MessageCodes.ADD_PRODUCTS, "Fruit$Apples$2000"));
		runUDPClient(testMessages);
	}

	public void thread4() {
		final List<Pair<MessageCodes, String>> testMessages = Arrays.asList(
			new Pair<>(MessageCodes.ADD_PRODUCTS, "Fruit$Apples$2000"));
		runUDPClient(testMessages);
	}

	public void thread5() {
		final List<Pair<MessageCodes, String>> testMessages = Arrays.asList(
			new Pair<>(MessageCodes.ADD_PRODUCTS, "Fruit$Apples$2000"));
		runUDPClient(testMessages);
	}

	public void thread6() {
		final List<Pair<MessageCodes, String>> testMessages = Arrays.asList(
			new Pair<>(MessageCodes.ADD_NEW_GROUP, "Sweets"),
			new Pair<>(MessageCodes.ADD_NEW_PRODUCT_TO_GROUP, "Sweets$Chocolate"),
			new Pair<>(MessageCodes.ADD_PRODUCTS, "Sweets$Chocolate$220"),
			new Pair<>(MessageCodes.SET_PRICE, "Sweets$Chocolate$65.8")
		);
		runUDPClient(testMessages);
	}

	public void thread7() {
		final List<Pair<MessageCodes, String>> testMessages = Arrays.asList(
			new Pair<>(MessageCodes.ADD_NEW_GROUP, "$$$"),
			new Pair<>(MessageCodes.SET_PRICE, "aaASDSAD$a#322F$PPPP$"),
			new Pair<>(MessageCodes.SUBTRACT_PRODUCTS, "Vegetables$Tomatoes$1000000")
		);
		runUDPClient(testMessages);
	}

	@Override
	public void finish() {
		System.out.println("Last client:");
		final List<Pair<MessageCodes, String>> testMessages = Arrays.asList(
			new Pair<>(MessageCodes.HOW_MANY, "Fruit$Apples"));

		runUDPClient(testMessages);

		System.out.println("\nServer stopped");
	}

	@Test
	public void udpTest() throws Throwable {
		TestFramework.runOnce(new UDPTest());
	}
}

