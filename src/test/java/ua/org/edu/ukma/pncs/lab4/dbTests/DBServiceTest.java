package ua.org.edu.ukma.pncs.lab4.dbTests;

import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import ua.org.edu.ukma.pncs.app.storage.model.Group;
import ua.org.edu.ukma.pncs.app.storage.model.Product;
import ua.org.edu.ukma.pncs.app.storage.db.DBHelper;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;
import ua.org.edu.ukma.pncs.app.storage.db.GroupService;
import ua.org.edu.ukma.pncs.app.storage.db.ProductService;

import java.util.List;
import java.util.Map;

public class DBServiceTest {

	private static final Group alloha = new Group("Alloha");
	private static final Group vegetables = new Group("Vegetables");
	private static final Group fruit = new Group("Fruit");

	private static final Product kokos = new Product(alloha, "Kokos", 100, 33.40);

	private static final Product tomatoes = new Product(vegetables, "Tomatoes", 333, 9.);
	private static final Product cucumbers = new Product(vegetables, "Cucumbers", 123, 12.);
	private static final Product potatoes = new Product(vegetables, "Potatoes", 234, 5.);
	private static final Product potatoes2 = new Product(fruit, "Potatoes2", 234, 5.);

	private static final Product mandarin = new Product(fruit, "Mandarin", 987, 12.90);

	private static DBService service;
	private static GroupService groupService;
	private static ProductService productService;

	private static final StringBuilder builder = new StringBuilder();

	@Rule
	public TestWatcher watchman = new TestWatcher() {

		@Override
		protected void failed(Throwable e, Description description) {
			if (description != null) {
				builder.append(description);
			}
			if (e != null) {
				builder.append(' ');
				builder.append(e);
			}
			builder.append(" FAIL\n");
		}

		@Override
		protected void succeeded(Description description) {
			if (description != null) {
				builder.append(description);
			}
			builder.append(" OK\n");
		}
	};

	@BeforeClass
	public static void setup() {
		DBHelper.isTest = true;
		DBHelper.dropTables = true;
		service = DBService.instantiateService(DBHelper.getConnection());
		groupService = service.getGroupService();
		productService = service.getProductService();
	}

	@Before
	public void init() {
		service.clear();
		groupService.createGroup(alloha.getName());
		groupService.createGroup(vegetables.getName());
		groupService.createGroup(fruit.getName());
		productService.createProduct(kokos);

		productService.createProduct(tomatoes);
		productService.createProduct(cucumbers);
		productService.createProduct(potatoes);
		productService.createProduct(potatoes2);

		productService.createProduct(mandarin);
		System.out.println("Initialized: " + service.getProductsToGroups());
	}

	@Test
	public void howManyTest() {
		Assert.assertEquals(kokos.getQuantity(), productService.howMany(kokos.getGroup().getName(), kokos.getName()));
		Assert.assertEquals(tomatoes.getQuantity(), productService.howMany(tomatoes.getGroup().getName(), tomatoes.getName()));
		Assert.assertEquals(cucumbers.getQuantity(), productService.howMany(cucumbers.getGroup().getName(), cucumbers.getName()));
		Assert.assertEquals(potatoes.getQuantity(), productService.howMany(potatoes.getGroup().getName(), potatoes.getName()));
		Assert.assertEquals(mandarin.getQuantity(), productService.howMany(mandarin.getGroup().getName(), mandarin.getName()));
	}

	@Test
	public void subtractProductsTest() {
		int subtract = 20;
		int before = productService.howMany(kokos.getGroup().getName(), kokos.getName());
		Assert.assertSame(productService.subtractProducts(kokos.getGroup().getName(), kokos.getName(), subtract), DBService.Result.SUCCESS);
		Assert.assertEquals(before - subtract, productService.howMany(kokos.getGroup().getName(), kokos.getName()).intValue());

		subtract = -20;
		Assert.assertSame(productService.subtractProducts(kokos.getGroup().getName(), kokos.getName(), subtract), DBService.Result.NEGATIVE_QUANTITY);

		subtract = 10000;
		Assert.assertSame(productService.subtractProducts(kokos.getGroup().getName(), kokos.getName(), subtract), DBService.Result.NOT_ENOUGH_PRODUCTS);
	}

	@Test
	public void plusProductsTest() {
		int plus = 200;
		int before = productService.howMany(tomatoes.getGroup().getName(), tomatoes.getName());
		Assert.assertSame(productService.plusProducts(tomatoes.getGroup().getName(), tomatoes.getName(), plus), DBService.Result.SUCCESS);
		Assert.assertEquals(before + plus, productService.howMany(tomatoes.getGroup().getName(), tomatoes.getName()).intValue());

		plus = -20;
		Assert.assertSame(productService.subtractProducts(tomatoes.getGroup().getName(), tomatoes.getName(), plus), DBService.Result.NEGATIVE_QUANTITY);
	}

	@Test
	public void AddGroupTest() {
		Group groupToAdd = new Group("Op Group");
		Assert.assertSame(groupService.createGroup(groupToAdd), DBService.Result.SUCCESS);
		Assert.assertSame(groupService.createGroup(groupToAdd), DBService.Result.FAILED);

		String stringGroupToAdd = "Models";
		Assert.assertSame(groupService.createGroup(stringGroupToAdd), DBService.Result.SUCCESS);
		Assert.assertSame(groupService.createGroup(stringGroupToAdd), DBService.Result.FAILED);

		Map<Group, List<Product>> groups = service.getProductsToGroups();
		System.out.println(groups);
		Assert.assertTrue(groups.containsKey(groupToAdd));
		Assert.assertTrue(groups.containsKey(new Group(stringGroupToAdd)));
	}

	@Test
	public void addProductToGroupTest() {
		Product productToAdd = new Product(alloha, "Avocado", 100, 40d);
		Assert.assertSame(productService.createProduct(productToAdd), DBService.Result.SUCCESS);
		Assert.assertSame(productService.createProduct(productToAdd), DBService.Result.FAILED);

		Product productToAdd2 = new Product(fruit, "Avocado2", 100, 40d);
		Assert.assertSame(productService.createProduct(productToAdd2), DBService.Result.SUCCESS);

		String stringProductToAdd = "Kivi";
		Assert.assertSame(productService.createProduct(stringProductToAdd, alloha.getName()), DBService.Result.SUCCESS);
		Assert.assertSame(productService.createProduct(stringProductToAdd, alloha.getName()), DBService.Result.FAILED);

		String stringProductToAdd_2 = "Carambola";
		String nonExistingGroup = "ahollA";
		Assert.assertSame(productService.createProduct(stringProductToAdd_2, nonExistingGroup), DBService.Result.FAILED);

		Map<Group, List<Product>> groups = service.getProductsToGroups();
		System.out.println(groups);
		Assert.assertTrue(groups.get(alloha).contains(productToAdd));
		Assert.assertTrue(groups.get(fruit).contains(productToAdd2));
		Assert.assertTrue(groups.get(alloha).contains(new Product(alloha, stringProductToAdd, 0, 0d)));
	}

	@Test
	public void setPriceTest() {
		Double price = 111.11;
		Double before = productService.getProduct(kokos.getGroup().getName(), kokos.getName()).getPrice();
		Assert.assertSame(productService.setPrice(kokos.getGroup().getName(), kokos.getName(), price), DBService.Result.SUCCESS);
		Assert.assertEquals(price, productService.getProduct(kokos.getGroup().getName(), kokos.getName()).getPrice());

		price = -11.11;
		Assert.assertSame(productService.setPrice(kokos.getGroup().getName(), kokos.getName(), price), DBService.Result.NEGATIVE_QUANTITY);
	}

	@Test
	public void deleteGroupTest() {
		String groupToDelete = vegetables.getName();

		Assert.assertSame(groupService.deleteGroup(alloha), DBService.Result.SUCCESS);
		Assert.assertSame(groupService.deleteGroup(alloha), DBService.Result.FAILED);
		Assert.assertSame(groupService.deleteGroup(""), DBService.Result.FAILED);
		Assert.assertSame(groupService.deleteGroup(groupToDelete), DBService.Result.SUCCESS);
		Map<Group, List<Product>> groups = service.getProductsToGroups();
		System.out.println(groups);
		Assert.assertFalse(groups.containsKey(alloha));
		Assert.assertEquals(groupService.getGroup(groupToDelete), new Group(""));
		Assert.assertNull(service.getProductsToGroup(groupToDelete).getKey());
	}

	@Test
	public void cascadeDeletingTest() {
		Group cascade = new Group("Cascade");
		Product prod1 = new Product(cascade, "1", 0, 0d);
		Product prod2 = new Product(cascade, "2", 0, 0d);
		Product prod3 = new Product(cascade, "3", 0, 0d);

		groupService.createGroup(cascade);
		productService.createProduct(prod1);
		productService.createProduct(prod2);
		productService.createProduct(prod3);

		Assert.assertEquals(productService.getProduct(prod1.getGroup().getName(), prod1.getName()), prod1);
		Assert.assertEquals(productService.getProduct(prod2.getGroup().getName(), prod2.getName()), prod2);
		Assert.assertEquals(productService.getProduct(prod3.getGroup().getName(), prod3.getName()), prod3);

		groupService.deleteGroup(cascade);

		Assert.assertNotEquals(productService.getProduct(prod1.getGroup().getName(), prod1.getName()), prod1);
		Assert.assertNotEquals(productService.getProduct(prod2.getGroup().getName(), prod2.getName()), prod2);
		Assert.assertNotEquals(productService.getProduct(prod3.getGroup().getName(), prod1.getName()), prod3);
	}

	@Test
	public void deleteProductTest() {
		Assert.assertSame(productService.deleteProduct(potatoes), DBService.Result.SUCCESS);
		Assert.assertSame(productService.deleteProduct(potatoes), DBService.Result.FAILED);
		Assert.assertSame(productService.deleteProduct(potatoes2), DBService.Result.SUCCESS);

		String nonExistingGroup = "ahollA";
		Assert.assertSame(productService.deleteProduct(mandarin.getName(), nonExistingGroup), DBService.Result.FAILED);

		Map<Group, List<Product>> groups = service.getProductsToGroups();
		System.out.println(groups);
		Assert.assertFalse(groups.get(vegetables).contains(potatoes));
		Assert.assertTrue(groups.get(fruit).contains(mandarin));
	}

	@Test
	public void multithreadingTest() {
		System.out.println("Mandarin: " + service.getProductsToGroup(fruit.getName()));

		int before = productService.howMany(alloha.getName(), kokos.getName());
		final int plus = 10;
		for (int i = 1; i <= 100; ++i) {
			new Thread(() -> productService.plusProducts(kokos.getGroup().getName(), kokos.getName(), plus)).start();
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int after = productService.howMany(alloha.getName(), kokos.getName());

		System.out.println("Before - " + before + "\nAfter - " + after);
		System.out.println("Kokos: " + productService.getProduct(kokos.getGroup().getName(), kokos.getName()));
	}

	@AfterClass
	public static void afterClass() {
		DBHelper.closeConnection();
		System.out.println(builder.toString());
	}

}
