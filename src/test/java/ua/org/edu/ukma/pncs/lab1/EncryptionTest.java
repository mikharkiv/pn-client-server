package ua.org.edu.ukma.pncs.lab1;

import org.junit.Assert;
import org.junit.Test;
import ua.org.edu.ukma.pncs.app.network.entity.Message;
import ua.org.edu.ukma.pncs.app.util.DESEncryptionManager;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public class EncryptionTest {

	public static final byte[] KEY = {31, 99, -8, 21, -39, -47, -73, 29};
	public static final byte[] KEY_1 = {30, 99, -8, 21, -39, -47, -73, 29};

	/**
	 * Method for testing string encryption with given key
	 * @param key
	 * @param message
	 */
	private void testStringEncryption(byte[] key, String message) {
		try {
			byte[] sourceBytes = message.getBytes(Charset.defaultCharset());
			byte[] encoded = DESEncryptionManager.encryptData(key, sourceBytes);
			byte[] decoded = DESEncryptionManager.decryptData(key, encoded);
			String encodedText = new String(encoded, Charset.defaultCharset());
			String decodedText = new String(decoded, Charset.defaultCharset());
			System.out.println("Source text - " + message);
			System.out.println("Encoded text - " + encodedText);
			System.out.println("Decoded text - " + decodedText);
			Assert.assertArrayEquals(sourceBytes, decoded);
			Assert.assertEquals(message, new String(decoded, Charset.defaultCharset()));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	/**
	 * Method for testing string encryption with different keys
	 * @param key_1
	 * @param key_2
	 * @param message
	 */
	private void testDifferentKeysEncryption(byte[] key_1, byte[] key_2, String message) {
		try {
			byte[] sourceBytes = message.getBytes(Charset.defaultCharset());
			byte[] encoded_1 = DESEncryptionManager.encryptData(key_1, sourceBytes);
			byte[] encoded_2 = DESEncryptionManager.encryptData(key_2, sourceBytes);
			byte[] decoded_1 = DESEncryptionManager.decryptData(key_1, encoded_1);
			byte[] decoded_2 = DESEncryptionManager.decryptData(key_2, encoded_2);
			System.out.println("Source bytes - " + message);
			System.out.println("Encoded bytes KEY 1 - " + new String(encoded_1, Charset.defaultCharset()));
			System.out.println("Encoded bytes KEY 2 - " + new String(encoded_2, Charset.defaultCharset()));

			Assert.assertArrayEquals(decoded_1, decoded_2);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	/**
	 * Test Packet encryption and decryption
	 */
	@Test
	public void test01Encryption() {
		Message msg = new Message(1, 1, "Test Message");
		byte[] msgBytes = new byte[0];
		msgBytes = msg.toByteArray();

		System.out.println("Source message - " + msg);
		System.out.println("Souce message in bytes representation - " + Arrays.toString(msgBytes));

		try {
			byte[] encodedMsgBytes = DESEncryptionManager.encryptData(KEY, msgBytes);
			System.out.println("Encrypted message in bytes representation - " + Arrays.toString(encodedMsgBytes));

			byte[] decodedMsgBytes = DESEncryptionManager.decryptData(KEY, encodedMsgBytes);
			System.out.println("Decrypted message in bytes representation - " + Arrays.toString(decodedMsgBytes));
			System.out.println("Source message - " + msg);
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	/**
	 * Test the correctness of encoding and decoding
	 * Test empty string encoding and decoding
	 */
	@Test
	public void test02Encryption() {
		System.out.println("\n=test02_1Deencode=");
		String sourceText = "test01Encryption";
		testStringEncryption(KEY, sourceText);

		System.out.println("\n=test02_2Deencode=");
		sourceText = "";
		testStringEncryption(KEY, sourceText);

		System.out.println("\n=test02_3Deencode=");
		sourceText = "ASDkkjkj354879HHHJJHJJJa90898293849898JJHFDSJFEWRWErew";
		testStringEncryption(KEY, sourceText);
	}

	/**
	 * Test equality of coding the same string with the same key
	 * Test difference of coding the same string with different keys
	 */
	@Test
	public void test03Encryption() {
		System.out.println("\n=test03_1Deencode=");

		String sourceText = "test03Encryption";
		testDifferentKeysEncryption(KEY, KEY, sourceText);

		System.out.println("\n=test02_2Deencode=");
		testDifferentKeysEncryption(KEY, KEY_1, sourceText);
	}


	/**
	 * Test decryption of a NON DES encrypted file
	 */
	@Test(expected = GeneralSecurityException.class)
	public void test04Encryption() throws IllegalArgumentException, GeneralSecurityException{
		System.out.println("\n=test04Deencode=");

		String sourceText = "DDSF334342S";
		System.out.println("Encoded text - " + sourceText);

		byte[] encoded = sourceText.getBytes();
		byte[] decoded = DESEncryptionManager.decryptData(KEY, encoded);

		String decodedText = new String(decoded, Charset.defaultCharset());
		System.out.println("Decoded text - " + decodedText);
	}
}
