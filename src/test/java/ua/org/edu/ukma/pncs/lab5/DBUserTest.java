package ua.org.edu.ukma.pncs.lab5;

import org.junit.*;
import ua.org.edu.ukma.pncs.app.storage.db.DBHelper;
import ua.org.edu.ukma.pncs.app.storage.db.DBService;
import ua.org.edu.ukma.pncs.app.storage.db.UserService;
import ua.org.edu.ukma.pncs.app.storage.model.User;

public class DBUserTest {

	private static UserService userService;

	@BeforeClass
	public static void setup() {
		DBHelper.isTest = true;
		DBHelper.dropTables = true;
		userService = DBService.instantiateService(DBHelper.getConnection()).getUserService();
	}

	@Before
	public void init() {
		userService.clear();
	}

	@Test
	public void userTests() {
		User testUser = new User("admin", "12345");
		Assert.assertEquals(userService.createUser(testUser), DBService.Result.SUCCESS);
		Assert.assertEquals(userService.createUser(testUser), DBService.Result.FAILED);

		Assert.assertEquals(userService.getUser(testUser.getLogin(), testUser.getPassword()), testUser);

		Assert.assertEquals(userService.deleteUser(testUser), DBService.Result.SUCCESS);
		Assert.assertEquals(userService.deleteUser(testUser), DBService.Result.FAILED);
	}

	@AfterClass
	public static void afterClass() {
		DBHelper.closeConnection();
	}
}
