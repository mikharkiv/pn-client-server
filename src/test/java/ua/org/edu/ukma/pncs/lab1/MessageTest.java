package ua.org.edu.ukma.pncs.lab1;

import org.junit.Assert;
import org.junit.Test;
import ua.org.edu.ukma.pncs.app.network.entity.Message;

public class MessageTest {

	@Test
	public void testMessage() {
		Message m1 = new Message();
		m1.setCType(1);
		m1.setBUserID(2);
		m1.setMessage("NaUKMA");
		Message m2 = new Message(1, 2, "NaUKMA");

		Assert.assertEquals(m1.getMessageSize(), 6);
		Assert.assertEquals(m2.getSize(), 14);
		Assert.assertEquals(m1, m2);
		Assert.assertEquals(m1.getSize(), m2.getSize());
		Assert.assertEquals(m1.getMessageSize(), m2.getMessageSize());
		Assert.assertArrayEquals(m1.toByteArray(), m2.toByteArray());
	}
}
